<?php
include '../assign4/login.php';

if (isset($_SESSION['login_user']))
{
    header("Location: welcome.php");
}
?>
<!DOCTYPE html>
<html>
    <head>
        <?php include '../head.php'; ?>
        <script>
            /*
             * Function to clear form
             */
            function clearTextArea()
            {
                document.getElementById('validation_area').innerHTML = "";
            } // End of function
        </script>
    </head>
    <body>
        <div id="perspective" class="perspective effect-airbnb">
            <?php include '../menu.php'; ?>
            <div class="container">
                <div class="wrapper"><!-- wrapper needed for scroll -->
                    <?php
                    $header_title = 'Assignment 5 - Address Book';
                    include '../header.php';
                    ?>
                    <div id="maintable">
                        <table style="width: 100%;">
                            <td class="maintable_info">
                                <h3 class="maintable_info_no_hover blog_box_border blog_box_glass">Please Login</h3>
                                <div class="blog_box_with_border">
                                    <br>
                                    <form action="" method="post" name="login_form">
                                        <div style="padding-left: 20px; padding-top: 10px; padding-right: 20px;">
                                            <h4 class="in_form">Username *</h4>
                                            <input type="text" name="username">
                                            <h4 class="in_form">Password *</h4> 
                                            <input type="password" name="password">
                                        </div>
                                        <h4 class="in_form_req_text">(*) Denotes Required Field</h4>
                                        <hr>
                                        <div style="text-align: center;">
                                            <input type="submit" value="Login">
                                            <input type="reset" value="Reset" onclick="clearTextArea();">
                                        </div>
                                        <br>
                                    </form>
                                    <br>
                                </div>
                                <hr>
                                <p class="maintable_info" style="color: red;" id="validation_area">
                                    <?php echo $error; ?>                                    
                                </p>
                                <p id="display_area">
                                </p>
                            </td>
                        </table>
                    </div>
                    <?php include '../footer.php'; ?>
                </div><!-- wrapper -->
            </div><!-- /container -->
            <?php include '../nav.php'; ?>
        </div><!-- /perspective -->
        <script src="../perspective/js/classie.js"></script>
        <script src="../perspective/js/menu.js"></script>
    </body>
</html>