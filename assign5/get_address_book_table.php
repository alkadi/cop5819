<?php

if (isset($login_session))
{

    $sql = "SELECT 1 FROM persons;";
    $val = mysqli_query($db, $sql);
    if ($val !== FALSE)
    {
        // Exists
        $query = "SELECT * FROM persons;";     // Display each record in the proper table
        $result = mysqli_query($db, $query);
        echo
        "
            <table class='shaded_table' style='width: 98%;' cellspacing='0'>
            <tr>
                <td class='as1'>First Name</td>
                <td class='as1'>Last Name</td>
                <td class='as1'>Gender</td>
                <td class='as1'>Phone Number</td>
                <td class='as1'>Address 1</td>
                <td class='as1'>Address 2</td>
                <td class='as1'>City</td>
                <td class='as1'>State</td>
                <td class='as1'>Zip</td>
            </tr>
        ";
        while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC))
        {
            echo
            "
                <tr>
                <td class='as2'>" . $row["FirstName_c"] . "</td>
                <td class='as2'>" . $row['LastName_c'] . "</td>
                <td class='as2'>" . $row['Gender_c'] . "</td>
                <td class='as2'>" . $row['PhoneNumber_c'] . "</td>
                <td class='as2'>" . $row['Address1_c'] . "</td>
                <td class='as2'>" . $row['Address2_c'] . "</td>
                <td class='as2'>" . $row['City_c'] . "</td>
                <td class='as2'>" . $row['State_c'] . "</td>
                <td class='as2'>" . $row['Zip_c'] . "</td>
                </tr>
            ";
        }
        echo "</table>";
    }
    else
    {
        echo "<div style='text-align: center;'>Address Book Empty!</div>";
    }
}
else
{
    // you're not logged in
    header("Location: index.php");
}
?>