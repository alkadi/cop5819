<?php
include('../assign4/lock.php');
$error = "";
$is_error = false;

if ($_SERVER["REQUEST_METHOD"] == "POST")
{
    // Get data from post
    $entry = $_POST['current_entry'];
    // only if valid stock value is present
    if ($entry != "")
    {
        mysqli_query($db, "DELETE FROM persons WHERE ID_n='" . addslashes($entry) . "'");
        header("Location: welcome.php");
    }
    else
    {
        $error = "* Something went wrong";
    }
}
?>
<!DOCTYPE html>
<html>
    <head>
        <?php include '../head.php'; ?>
        <script>
            /*
             * Function to clear form
             */
            function clearTextArea()
            {
                document.getElementById('validation_area').innerHTML = "";
            } // End of function
        </script>
    </head>
    <body>
        <div id="perspective" class="perspective effect-airbnb">
            <?php include '../menu.php'; ?>
            <div class="container">
                <div class="wrapper"><!-- wrapper needed for scroll -->
                    <?php
                    $header_title = 'Assignment 5 - Contacts Book: Delete Entry';
                    include '../header.php';
                    ?>
                    <div id="maintable">
                        <table style="width: 100%;">
                            <td class="maintable_info">
                                <?php include('get_sub_menu.php'); ?>
                                <h3 class="maintable_info_no_hover blog_box_border blog_box_glass">Delete entry</h3>
                                <div class="blog_box_with_border">
                                    <br>
                                    <form action="" method="post" name="delete_entry_form">
                                        <div style="padding-left: 20px; padding-top: 10px; padding-right: 20px;">
                                            <h4 class="in_form">Entry to delete</h4>
                                            <select name="current_entry" style="width: 100%;">
                                                <?php
                                                $sql = "SELECT 1 FROM persons;";
                                                $val = mysqli_query($db, $sql);
                                                if ($val !== FALSE)
                                                {
                                                    // Exists
                                                    $query = "SELECT * FROM persons;";
                                                    $result = mysqli_query($db, $query);
                                                    while ($row = mysqli_fetch_array($result, MYSQLI_NUM))
                                                    {
                                                        echo "<option value='" . $row[0] . "'>"
                                                        . $row[0] . " - "
                                                        . $row[1] . " "
                                                        . $row[2] . "</option>";
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                        <h4 class="in_form_req_text"><!--(*) Denotes Required Field--></h4>
                                        <hr>
                                        <div style="text-align: center;">
                                            <input type="submit" value="Delete">
                                            <!--<input type="reset" value="Reset" onclick="clearTextArea();">-->
                                        </div>
                                        <br>
                                    </form>
                                    <br>
                                </div>
                                <hr>
                                <p class="maintable_info" style="color: red;" id="validation_area">
                                    <?php echo $error; ?>                                    
                                </p>
                                <p id="display_area">
                                </p>
                            </td>
                        </table>
                    </div>
                    <?php include '../footer.php'; ?>
                </div><!-- wrapper -->
            </div><!-- /container -->
            <?php include '../nav.php'; ?>
        </div><!-- /perspective -->
        <script src="../perspective/js/classie.js"></script>
        <script src="../perspective/js/menu.js"></script>
    </body>
</html>