<?php
include('../assign4/lock.php');
$error = "";
$is_error = false;

// Check if we have a variable from session so we can pre-populate the form
if (isset($_SESSION['modify_entry']))
{
    $do_update = true;
    $row = $_SESSION['modify_entry'];
    $id = $row['ID_n'];
    $first_name = $row['FirstName_c'];
    $last_name = $row['LastName_c'];
    $gender = $row['Gender_c'];
    $phone_number = $row['PhoneNumber_c'];
    $address_line_1 = $row['Address1_c'];
    $address_line_2 = $row['Address2_c'];
    $city = $row['City_c'];
    $state = $row['State_c'];
    $zip_code = $row['Zip_c'];
}
else
{
    $do_update = false;
    $id = '';
    $first_name = '';
    $last_name = '';
    $gender = 'male';
    $phone_number = '';
    $address_line_1 = '';
    $address_line_2 = '';
    $city = '';
    $state = '';
    $zip_code = '';
}


if ($_SERVER["REQUEST_METHOD"] == "POST")
{
    // Get data from post
    $first_name = $_POST['first_name'];
    if ($first_name == '' || ctype_digit($first_name) || $first_name == null)
    {
        $error.= "* First Name Error, must be provided with no numbers<br>";
        $is_error = true;
    }

    $last_name = $_POST['last_name'];
    if ($last_name == '' || ctype_digit($last_name) || $last_name == null)
    {
        $error.= "* Last Name Error, must be provided with no numbers<br>";
        $is_error = true;
    }

    $gender = $_POST['sex'];

    $phone_number = $_POST['phone_number'];
    if ($phone_number == '' || !ctype_digit($phone_number) || $phone_number == null)
    {
        $error.= "* Phone Number Error, must be provided and must be a numbers<br>";
        $is_error = true;
    }

    $address_line_1 = $_POST['address_line_1'];
    $address_line_2 = $_POST['address_line_2'];
    $city = $_POST['city'];
    $state = $_POST['DD_states'];
    $zip_code = $_POST['zip_code'];

    // only if validation passes
    if ($is_error == false)
    {
        $sql = "SELECT 1 FROM persons;";
        $val = mysqli_query($db, $sql);
        if ($val !== FALSE)
        {
            // Exists, do nothing
        }
        else
        {
            // I can't find it...
            $sql = "
                CREATE TABLE persons
                (
                    ID_n INT NOT NULL AUTO_INCREMENT,
                    PRIMARY KEY (ID_n),	
                    FirstName_c CHAR(255),
                    LastName_c CHAR(255),
                    Gender_c CHAR(8),
                    PhoneNumber_c CHAR(64),
                    Address1_c CHAR(128),
                    Address2_c CHAR(128),
                    City_c CHAR(128),
                    State_c CHAR(128),
                    Zip_c CHAR(16)
               );
            ";
            if (!mysqli_query($db, $sql))
            {
                echo '<script language="javascript">';
                echo 'alert("Table Creation Error!")';
                echo '</script>';
            }
        } // End of create table block

        if ($do_update == true)
        {
            $sql = " 
                UPDATE persons SET
                FirstName_c = '" . addslashes($first_name) . "', 
                LastName_c = '" . addslashes($last_name) . "', 
                Gender_c = '" . addslashes($gender) . "', 
                PhoneNumber_c = '" . addslashes($phone_number) . "', 
                Address1_c = '" . addslashes($address_line_1) . "', 
                Address2_c = '" . addslashes($address_line_2) . "', 
                City_c = '" . addslashes($city) . "', 
                State_c = '" . addslashes($state) . "', 
                Zip_c = '" . addslashes($zip_code) . "'
                WHERE ID_n='" . addslashes($id) . "'
            ";
        }
        else
        {
            $sql = " 
                INSERT INTO persons 
                (
                    FirstName_c, LastName_c, Gender_c, PhoneNumber_c, 
                    Address1_c, Address2_c, City_c, State_c, Zip_c
                )
                VALUES 
                (
                    '" . addslashes($first_name) . "', 
                    '" . addslashes($last_name) . "', 
                    '" . addslashes($gender) . "', 
                    '" . addslashes($phone_number) . "', 
                    '" . addslashes($address_line_1) . "', 
                    '" . addslashes($address_line_2) . "', 
                    '" . addslashes($city) . "', 
                    '" . addslashes($state) . "', 
                    '" . addslashes($zip_code) . "'
                );
            ";
        }

        if (!mysqli_query($db, $sql))
        {
            echo '<script language="javascript">';
            echo 'alert("Entry Creation Error!")';
            echo '</script>';
        }
        else
        {
            // We are done
            if (isset($_SESSION['modify_entry']))
            {
                unset($_SESSION['modify_entry']);
            }
            header('Location: welcome.php');
        }
    }
}
?>
<!DOCTYPE html>
<html>
    <head>
        <?php include '../head.php'; ?>
        <script>
            /*
             * Function to clear form
             */
            function clearTextArea()
            {
                document.getElementById('validation_area').innerHTML = "";
            } // End of function
        </script>
    </head>
    <body>
        <div id="perspective" class="perspective effect-airbnb">
            <?php include '../menu.php'; ?>
            <div class="container">
                <div class="wrapper"><!-- wrapper needed for scroll -->
                    <?php
                    $header_title = 'Assignment 5 - Contacts Book: Add or Update Entry';
                    include '../header.php';
                    ?>
                    <div id="maintable">
                        <table style="width: 100%;">
                            <td class="maintable_info">
                                <?php include('get_sub_menu.php'); ?>
                                <h3 class="maintable_info_no_hover blog_box_border blog_box_glass">Add or update entry</h3>
                                <div class="blog_box_with_border">
                                    <br>
                                    <form action="" method="post" name="add_entry_form">
                                        <div style="padding-left: 20px; padding-top: 10px; padding-right: 20px;">
                                            <h4 class="in_form">First Name *</h4>
                                            <input type="text" name="first_name" value="<?php echo $first_name ?>">
                                            <h4 class="in_form">Last Name *</h4> 
                                            <input type="text" name="last_name" value="<?php echo $last_name ?>">
                                            <h4 class="in_form">Gender *</h4> 
                                            <input type="radio" name="sex" value="male" 
                                            <?php
                                            if ($gender == 'male')
                                            {
                                                echo 'checked';
                                            }
                                            ?> 
                                                   >Male
                                            <input type="radio" name="sex" value="female"
                                            <?php
                                            if ($gender == 'female')
                                            {
                                                echo 'checked';
                                            }
                                            ?> 
                                                   >Female
                                            <h4 class="in_form">Phone Number *</h4>
                                            <input type="text" name="phone_number" value="<?php echo $phone_number ?>">
                                            <h4 class="in_form">Address Line 1</h4>
                                            <input type="text" name="address_line_1" value="<?php echo $address_line_1 ?>">
                                            <h4 class="in_form">Address Line 2</h4>
                                            <input type="text" name="address_line_2" value="<?php echo $address_line_2 ?>">
                                            <h4 class="in_form">City</h4>
                                            <input type="text" name="city" value="<?php echo $city ?>">
                                            <h4 class="in_form">State</h4>
                                            <?php include('get_states_list.php'); ?>
                                            <script>
                                                var myvar = <?php echo json_encode($state); ?>;
                                                for (var i = 0; i < document.getElementById("DD_states").length; i++)
                                                {
                                                    if (document.getElementById("DD_states").options[i].value === myvar)
                                                    {
                                                        document.getElementById("DD_states").selectedIndex = i;
                                                    }
                                                }
                                            </script>
                                            <h4 class="in_form">Zipe Code</h4>
                                            <input type="text" name="zip_code" value="<?php echo $zip_code ?>">
                                        </div>
                                        <h4 class="in_form_req_text">(*) Denotes Required Field</h4>
                                        <hr>
                                        <div style="text-align: center;">
                                            <input type="submit" value="<?php echo($do_update ? "Update" : "Add"); ?>">
                                            <input type="reset" value="Reset" onclick="clearTextArea();">
                                        </div>
                                        <br>
                                    </form>
                                    <br>
                                </div>
                                <hr>
                                <p class="maintable_info" style="color: red;" id="validation_area">
                                    <?php echo $error; ?>                                    
                                </p>
                                <p id="display_area">
                                </p>
                            </td>
                        </table>
                    </div>
                    <?php include '../footer.php'; ?>
                </div><!-- wrapper -->
            </div><!-- /container -->
            <?php include '../nav.php'; ?>
        </div><!-- /perspective -->
        <script src="../perspective/js/classie.js"></script>
        <script src="../perspective/js/menu.js"></script>
    </body>
</html>