<!DOCTYPE html>

<html>
    <head>
        <?php include 'head.php'; ?>        
    </head>
    <body>
        <div id="perspective" class="perspective effect-airbnb">
            <?php include 'menu.php'; ?>
            <div class="container">
                <div class="wrapper"><!-- wrapper needed for scroll -->
                    <?php
                    $header_title = 'Content Coming Soon! - COP 5819';
                    include './header.php';
                    ?>                                        
                    <div id="maintable">
                        <table style="width: 100%;">
                            <td class="maintable_info">
                                <h3 class="maintable_info_no_hover blog_box_border blog_box_glass">Under Construction</h3>
                                <div class="blog_box_with_border">
                                    <br>
                                    <?php include 'rss_feed.php'; ?>
                                    <br>
                                </div>
                                <hr>
                            </td>
                        </table>
                    </div>
                    <?php include 'footer.php'; ?>
                </div><!-- wrapper -->
            </div><!-- /container -->
            <?php include 'nav.php'; ?>
        </div><!-- /perspective -->
        <script src="perspective/js/classie.js"></script>
        <script src="perspective/js/menu.js"></script>
    </body>
</html>