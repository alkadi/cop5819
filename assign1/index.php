<!DOCTYPE html>

<html>
    <head>
        <?php include '../head.php'; ?>
    </head>
    <body>
        <div id="perspective" class="perspective effect-airbnb">
            <?php include '../menu.php'; ?>
            <div class="container">
                <div class="wrapper"><!-- wrapper needed for scroll -->
                    <?php
                    $header_title = 'Assignment 1 - About Me';
                    include '../header.php';
                    ?>                    
                    <div id="maintable">
                        <table style="width: 100%;">
                            <td class="maintable_info">
                                <h3 class="maintable_info_no_hover blog_box_border blog_box_glass">Al Alkadi, BSEE</h3>
                                <div class="blog_box_with_border">
                                    <br>
                                    <img class="profile_image" align="left" width="150" hspace="50"
                                         src="me.jpg" alt="Al Alkadi's Profile Picture">
                                    <p class="maintable_info">
                                        My name is Al Alkadi and I graduated from UNF's Electrical Engineering program in 2011
                                        with a bachelor's degree. I am currently a full time research and development software 
                                        engineer, I don't generally do much web programming on daily basis, but I deal with PHP
                                        almost every day! I am looking forward to learning more about web development from 
                                        this class!
                                    </p>
                                    <p class="maintable_info">
                                        As a research and development C++ software engineer my responsibilities include designing, 
                                        implementing, and extending a backbone framework for advanced security applications by 
                                        integrating various technologies. These technologies include web and mobile GUI 
                                        applications; video capture, analysis, archiving and streaming; PLC and other automation 
                                        controllers; IP based sensors and output devices; custom drivers, third party SDKs, and 
                                        open source projects. Responsibilities, scope of work, and expertise include:                        
                                    </p>
                                    <ul class="maintable_info">
                                        <li>Cross-platform development (Linux & Windows) in C++</li>
                                        <li>32 and 64 bit platforms compatibility</li>
                                        <li>Video and image processing and analysis using OpenCV in C++</li>
                                        <li>Video management systems development</li>
                                        <li>Continuous integration servers implementation</li>
                                        <li>Large code-base management with custom build servers</li>
                                        <li>Repository maintenance and issue tracking</li>
                                        <li>Web services integration</li>
                                        <li>Camera driver development</li>
                                        <li>Hardware driver development and user interface design</li>
                                        <li>Cross-application communication standardization</li>
                                        <li>Open-source code management</li>
                                    </ul>
                                </div>
                                <hr>
                                <h3 class="maintable_info_no_hover blog_box_border blog_box_glass">My current graduate UNF classes</h3>
                                <div class="blog_box_with_border">
                                    <br>
                                    <table class="shaded_table" cellspacing="0">
                                        <tr>
                                            <td class="as1">Course Number</td>
                                            <td class="as1">Course Name</td>		
                                            <td class="as1">Course Time</td>
                                        </tr>
                                        <tr>
                                            <td class="as2">COP 5819</td>
                                            <td class="as2">Internet Programming</td>		
                                            <td class="as2">ONLINE</td>
                                        </tr>
                                    </table>
                                    <br>
                                </div>
                                <hr>
                                <h3 class="maintable_info_no_hover blog_box_border blog_box_glass">My favorite sites</h3>
                                <div class="blog_box_with_border">
                                    <ul class="maintable_info">
                                        <li><a class="footer" href="http://www.linkedin.com/in/alalkadi" target="_blank">My LinkedIn</a></li>
                                        <li><a class="footer" href="https://twitter.com/Al_TheEngineer" target="_blank">My Twitter</a></li>
                                        <li><a class="footer" href="http://www.aj-electronics.com/" target="_blank">Android Smart Home Project</a></li>
                                    </ul>
                                </div>
                                <hr>
                            </td>
                        </table>
                    </div>
                    <?php include '../footer.php'; ?>
                </div><!-- wrapper -->
            </div><!-- /container -->
            <?php include '../nav.php'; ?>
        </div><!-- /perspective -->
        <script src="../perspective/js/classie.js"></script>
        <script src="../perspective/js/menu.js"></script>
    </body>
</html>