<!DOCTYPE html>

<html>
    <head>
        <?php include '../head.php'; ?>
        <script src="statistics.js"></script>
        <script>
            /*
             * Function to clear form
             */
            function clearTextArea()
            {
                document.getElementById("number_list").value = "";
                document.getElementById('display_area').innerHTML = "";
                document.getElementById('validation_area').innerHTML = "";
            } // End of function

            /*
             * Function to validate form
             */
            function validateForm()
            {
                // Variables
                var is_error = false;
                var error_msg = "Form Errors:<br>";

                // Validate content of array list
                var content = document.getElementById("number_list").value;
                if (content !== null && content !== "" && content.length > 1)
                {
                    // Make our array, and check that all its contents are numbers
                    var array_list = content.split(/[,.!?;\s\r\n]\s*/g);
                    if (array_list[array_list.length - 1] === "")
                    {
                        array_list.pop();
                    }
                    for (var i = 0; i < array_list.length; i++)
                    {
                        if (isNaN(array_list[i]))
                        {
                            error_msg += " * List item [" + (i + 1).toString() + "] '" + array_list[i] + "' is not a number<br>";
                            is_error = true;
                        }
                        else
                        {
                            array_list[i] = parseInt(array_list[i], 10);
                        }
                    }
                }
                else
                {
                    if (content.length === 1)
                    {
                        error_msg += " * Form has only one item<br>";
                    }
                    else
                    {
                        error_msg += " * Form is empty<br>";
                    }
                    is_error = true;
                } // I guess you didn't type anything!

                if (is_error === true)
                {
                    document.getElementById('validation_area').innerHTML = error_msg;
                }
                else
                {
                    // No error
                    document.getElementById('validation_area').innerHTML = "";
                    displayResults(array_list);
                }
            } // End of function

            /*
             * Function to display the results into form
             */
            function displayResults(array)
            {
                var display_body = "<h3 class='maintable_info_no_hover blog_box_border blog_box_glass'>Results</h3>";
                display_body += "<div class='blog_box_with_border'>";

                display_body += "<h3 class='maintable_info_no_hover'>Set:</h3>";
                display_body += "<p class='maintable_info'>{" + array.toString() + "}</p>";

                display_body += "<h3 class='maintable_info_no_hover'>Summation:</h3>";
                display_body += "<p class='maintable_info'>" + ((findSum(array)).toFixed(2)).toString() + "</p>";

                display_body += "<h3 class='maintable_info_no_hover'>Mean:</h3>";
                display_body += "<p class='maintable_info'>" + ((findMean(array)).toFixed(2)).toString() + "</p>";

                display_body += "<h3 class='maintable_info_no_hover'>Median:</h3>";
                display_body += "<p class='maintable_info'>" + ((findMedian(array)).toFixed(2)).toString() + "</p>";

                display_body += "<h3 class='maintable_info_no_hover'>Mode:</h3>";
                display_body += "<p class='maintable_info'>" + (findMode(array)).toString() + "</p>";

                display_body += "<h3 class='maintable_info_no_hover'>Variance:</h3>";
                display_body += "<p class='maintable_info'>" + ((findVariance(array)).toFixed(2)).toString() + "</p>";

                display_body += "<h3 class='maintable_info_no_hover'>Standard Deviation:</h3>";
                display_body += "<p class='maintable_info'>" + ((findStandardDeviation(array)).toFixed(2)).toString() + "</p>";

                display_body += "</div><hr>";
                document.getElementById('display_area').innerHTML = display_body;
            }
        </script>
    </head>
    <body>
        <div id="perspective" class="perspective effect-airbnb">
            <?php include '../menu.php'; ?>
            <div class="container">
                <div class="wrapper"><!-- wrapper needed for scroll -->
                    <?php
                    $header_title = 'Assignment 3 - Statistics Using A Java Script Library';
                    include '../header.php';
                    ?>                    
                    <div id="maintable">
                        <table style="width: 100%;">
                            <td class="maintable_info">
                                <h3 class="maintable_info_no_hover blog_box_border blog_box_glass">Enter positive or negative integers separated 
                                    by spaces or any punctuation</h3>
                                <div class="blog_box_with_border">
                                    <br>
                                    <form name="numbers_form" onsubmit="validateForm();
                                            return false;">
                                        <div style="padding-left: 20px; padding-top: 10px; padding-right: 20px;">
                                            <h4 class="in_form">List of Numbers (whitespace will be removed) *</h4>
                                            <textarea class="in_form" id="number_list" form="numbers_form"></textarea>
                                        </div>
                                        <h4 class="in_form_req_text">(*) Denotes Required Field</h4>
                                        <hr>
                                        <div style="text-align: center;">
                                            <input type="submit" value="Submit">
                                            <input type="reset" value="Reset" onclick="clearTextArea();">
                                        </div>
                                        <br>
                                    </form>
                                    <br>
                                    <a class="maintable_info" href="tests_assign3.pdf" target="_blank">(Click here for test results)</a>
                                    <br>
                                    <br>
                                </div>
                                <hr>
                                <p class="maintable_info" style="color: red;" id="validation_area">
                                </p>              
                                <p id="display_area">
                                </p>                                                
                            </td>
                        </table>
                    </div>
                    <?php include '../footer.php'; ?>
                </div><!-- wrapper -->
            </div><!-- /container -->
            <?php include '../nav.php'; ?>
        </div><!-- /perspective -->
        <script src="../perspective/js/classie.js"></script>
        <script src="../perspective/js/menu.js"></script>
    </body>
</html>