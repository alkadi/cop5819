// Statistics Java Script File, generally takes an array of numbers for processing

/*
 * Function finds number of elements in the array
 */
function findN(array)
{
    return array.length;
} // End of function

/*
 * Return the summation of the array
 */
function findSum(array)
{
    var sum = 0;
    for (var i = 0; i < array.length; i++)
    {
        sum += parseInt(array[i]);
    }
    return sum;
} // End of function

/*
 * Returns the mean of a numerical array
 */
function findMean(array)
{
    return findSum(array) / findN(array);
} // End of function

/*
 * Returns the median of a numerical array
 */
function findMedian(array)
{
    array.sort
            (
                    function(a, b)
                    {
                        return a - b;
                    }
            );

    var mid = Math.floor(array.length / 2);

    if (array.length % 2)
    {
        return array[mid];
    }
    else
    {
        return (array[mid - 1] + array[mid]) / 2.0;
    }
} // End of function

/*
 * Returns comma separated mode(s) of a numerical array, should be O(N) execution time
 */
function findMode(array)
{
    // Internal variables, currently the output is string
    var output_str = "";
    var counts = [0];
    var counts_neg = [0];
    var max_value = Math.max.apply(null, array);
    var abs_max_value = Math.abs(Math.min.apply(null, array));
    
    // Grab the largest value for allocation
    if (abs_max_value > max_value)
    {
        max_value = abs_max_value; // This is the maximum memory address to allocate
    }

    // This is rather annoying, but we need to clear the array to 0's since JS is not typed
    for (var i = 0; i <= max_value; i++)
    {
        counts[i] = 0; // Init array element to int 0
        counts_neg[i] = 0; // Init array element to int 0, for negatives
    }
    // Add +1 for every occurrence of a value in the respective position of the array
    for (var i = 0; i < array.length; i++)
    {
        if (array[i] < 0) // A negative number
        {
            counts_neg[Math.abs(array[i])]++;
        }
        else // A positive number
        {
            counts[array[i]]++;
        }
    }
    // Deterime the highest count
    var highest_index = Math.max.apply(null, counts);
    var highest_index_neg = Math.max.apply(null, counts_neg);
    // Decide which index value to use
    if (highest_index_neg > highest_index)
    {
        highest_index = highest_index_neg; // who's got the highest frequency?
    }
    // Now find any other occurrences, only if we have frequency more than 1
    if (highest_index > 1)
    {
        for (var i = 0; i < counts.length; i++) // this is safe, both arrays have same length
        {
            if (counts_neg[i] === highest_index)
            {
                output_str += "-" + i.toString() + ", ";
            }
            if (counts[i] === highest_index)
            {
                output_str += i.toString() + ", ";
            }
        }
        // Just remove a training comma and space if a mode is found
        if (output_str !== "")
        {
            output_str = output_str.slice(0, -2); // remove trailig comma
            return output_str;
        }
    }
    else
    {
        return "None";
    }
} // End of function

/*
 * Returns variance of a sample
 */
function findVariance(array)
{
    var sum = 0;
    var mean = findMean(array);
    var n = findN(array);
    for (var i = 0; i < n; i++)
    {
        var diff = array[i] - mean;
        sum += Math.pow(diff, 2);
    }
    sum *= (1 / (n - 1));
    return sum;
} // End of function

/*
 * Returns Standard Deviation of a sample
 */
function findStandardDeviation(array)
{
    return Math.sqrt(findVariance(array));
} // End of function