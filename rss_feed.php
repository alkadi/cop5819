<div class="shaded_div">
    <!-- start feedwind code -->
    <script type="text/javascript">
        document.write('<script type="text/javascript" src="' + ('https:' == document.location.protocol ? 'https://'
                : 'http://') + 'feed.mikle.com/js/rssmikle.js"><\/script>');
    </script>
    <script type="text/javascript">
        (function()
        {
            var params = {rssmikle_url: "https://bitbucket.org/alkadi/cop5819/rss", rssmikle_frame_width: "648",
                rssmikle_frame_height: "256", rssmikle_target: "_blank", rssmikle_font: "Arial, Helvetica, sans-serif",
                rssmikle_font_size: "12", rssmikle_border: "on", responsive: "off", rssmikle_css_url: "",
                text_align: "left", text_align2: "justify", corner: "off", autoscroll: "on", scrolldirection: "up",
                scrollstep: "5", mcspeed: "30", sort: "Off", rssmikle_title: "on",
                rssmikle_title_sentence: "Website Code Repository Updates",
                rssmikle_title_link: "https://bitbucket.org/alkadi/cop5819/commits/all",
                rssmikle_title_bgcolor: "#CEDEFF", rssmikle_title_color: "#000099", rssmikle_title_bgimage: "",
                rssmikle_item_bgcolor: "#FFFFFF", rssmikle_item_bgimage: "", rssmikle_item_title_length: "64",
                rssmikle_item_title_color: "#000099", rssmikle_item_border_bottom: "on", rssmikle_item_description: "on",
                rssmikle_item_description_length: "256", rssmikle_item_description_color: "#000000",
                rssmikle_item_date: "gl1", rssmikle_timezone: "Etc/GMT", datetime_format: "%b %e, %Y %l:%M:%S %p",
                rssmikle_item_description_tag: "off", rssmikle_item_description_image_scaling: "off", article_num: "15",
                rssmikle_item_podcast: "off", keyword_inc: "", keyword_exc: "", };
            feedwind_show_widget_iframe(params);
        }
        )();
    </script>
    <div style="font-size:10px; text-align:center; width:650;">
        <a href="http://feed.mikle.com/" target="_blank" style="color:#CCCCCC;">RSS Feed Widget</a>
        <!--Please display the above link in your web page according to Terms of Service.-->
    </div>
    <!-- end feedwind code -->
</div>