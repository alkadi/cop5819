<div id="footer">
    <h6 class="footer">
        <a class="footer" href="http://www.linkedin.com/in/alalkadi" target="_blank">My LinkedIn</a> | 
        <a class="footer" href="https://twitter.com/Al_TheEngineer" target="_blank">My Twitter</a> | 
        <a class="footer" href="http://www.aj-electronics.com/" target="_blank">Android Smart Home Project</a>
    </h6>
</div>
<div class="end_of_page">
    Designed by: Al M. Alkadi | Best viewed in FireFox
</div>