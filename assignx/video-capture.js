function successCallback(stream)
{
    var video = document.querySelector("video");
    var canvas = document.querySelector('canvas');

    window.stream = stream; // stream available to console
    if (window.URL)
    {
        video.src = window.URL.createObjectURL(stream);
    }
    else
    {
        video.src = stream;
    }
    
    setInterval(function()
    {
        onFrame(video, canvas);
    }, 33);
}

function errorCallback(error)
{
    console.log("navigator.getUserMedia error: ", error);
}

function captureVideo()
{
    // Ger browswer user media (video)
    var constraints = {audio: false, video: true};
    navigator.getUserMedia = navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia;
    navigator.getUserMedia(constraints, successCallback, errorCallback);
}

function onFrame(video, canvas)
{
    var ctx = canvas.getContext('2d');
    var cw = video.videoWidth;
    var ch = video.videoHeight;

    canvas.width = cw;
    canvas.height = ch;
    ctx.drawImage(video, 0, 0, cw, ch);
}