<?php

if (isset($login_session))
{
    $_SESSION['index_path'] = '/~n00644504/cop4813/assign4'; //dirname(__FILE__); // Needed redirection
    echo
    "
        <h3 class='maintable_info_no_hover blog_box_border blog_box_glass'>
            Welcome: $login_session! What would you like to do?</h3>
        <div class='blog_box_with_border'>
            <br>
            <div style='text-align: center;'>
                <a href='welcome.php'>Home</a> | 
                <a href='add_stock.php'>Add Stock</a> | 
                <a href='delete_stock.php'>Delete Stock</a> | 
                <a href='modify_stock.php'>Modify Stock</a> |
                <a href='logout.php'>Logout</a>
            </div>
            <br>
        </div>
        <hr>
    ";
}
else
{
    // you're not logged in
    header("Location: index.php");
}
?>