<?php

session_start();
$index_path = $_SESSION['index_path'];

if (isset($_SESSION['login_user']))
{
    unset($_SESSION['login_user']);
    unset($_SESSION['index_path']);
    header('Location:' . $index_path . '/index.php');
}
?>