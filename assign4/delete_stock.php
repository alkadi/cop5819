<?php
include('lock.php');
$error = "";
if ($_SERVER["REQUEST_METHOD"] == "POST")
{
    // Get data from post
    $entry = $_POST['current_stock'];

    // only if valid stock value is present
    if ($entry != "")
    {
        // Check if stock already exists, then delete it
        $csv_array = array_map('str_getcsv', file('stocks.csv'));
        foreach ($csv_array as $key => $value)
        {
            if ($value[0] == $entry)
            {
                unset($csv_array[$key]);
            }
        }

        // Re-write file with modified values
        $fp = fopen('stocks.csv', 'w');
        foreach ($csv_array as $key => $value)
        {
            fputcsv($fp, $value);
        }
        fclose($fp);
        header("Location: welcome.php");
    }
    else
    {
        $error = "* Something went wrong";
    }
}
?>
<!DOCTYPE html>
<html>
    <head>
        <?php include '../head.php'; ?>
        <script>
            /*
             * Function to clear form
             */
            function clearTextArea()
            {
                document.getElementById('validation_area').innerHTML = "";
            } // End of function
        </script>
    </head>
    <body>
        <div id="perspective" class="perspective effect-airbnb">
            <?php include '../menu.php'; ?>
            <div class="container">
                <div class="wrapper"><!-- wrapper needed for scroll -->
                    <?php
                    $header_title = 'Assignment 4 - Stock Portfolio: Delete Stock';
                    include '../header.php';
                    ?>
                    <div id="maintable">
                        <table style="width: 100%;">
                            <td class="maintable_info">
                                <?php include('get_sub_menu.php'); ?>
                                <h3 class="maintable_info_no_hover blog_box_border blog_box_glass">Delete stock</h3>
                                <div class="blog_box_with_border">
                                    <br>
                                    <form action="" method="post" name="delete_stock_form">
                                        <div style="padding-left: 20px; padding-top: 10px; padding-right: 20px;">
                                            <h4 class="in_form">Stock to delete</h4>
                                            <select name="current_stock" style="width: 100%;">
                                                <?php
                                                $csv_array = array_map('str_getcsv', file('stocks.csv'));
                                                foreach ($csv_array as $key => $value)
                                                {
                                                    echo "<option value='$value[0]'>$value[0]</option>";
                                                }
                                                ?>
                                            </select>
                                        </div>
                                        <h4 class="in_form_req_text"><!--(*) Denotes Required Field--></h4>
                                        <hr>
                                        <div style="text-align: center;">
                                            <input type="submit" value="Delete">
                                            <!--<input type="reset" value="Reset" onclick="clearTextArea();">-->
                                        </div>
                                        <br>
                                    </form>
                                    <br>
                                </div>
                                <hr>
                                <p class="maintable_info" style="color: red;" id="validation_area">
                                    <?php echo $error; ?>                                    
                                </p>
                                <p id="display_area">
                                </p>
                            </td>
                        </table>
                    </div>
                    <?php include '../footer.php'; ?>
                </div><!-- wrapper -->
            </div><!-- /container -->
            <?php include '../nav.php'; ?>
        </div><!-- /perspective -->
        <script src="../perspective/js/classie.js"></script>
        <script src="../perspective/js/menu.js"></script>
    </body>
</html>