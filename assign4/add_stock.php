<?php
include('lock.php');
$error = "";
$stocks_file = 'stocks.csv';

if ($_SERVER["REQUEST_METHOD"] == "POST")
{
    // Get data from post
    $entry = $_POST['stock_symbol'];
    $shares_amount = $_POST['shares_amount'];

    // Read the main CSV file of stocks into an array, this is bad if the file is HUGE!
    $file_yahoo_api = 'http://finance.yahoo.com/d/quotes.csv?s=' . $entry . '&f=sl1d1t1c1ohgv&e=.csv';
    $csv_from_yahoo = array_map('str_getcsv', file($file_yahoo_api));

    // only if valid stock value is present
    if ($csv_from_yahoo[0][1] != "0.00" && $shares_amount > 0)
    {
        // Get proper name
        $entry = $csv_from_yahoo[0][0];

        // Check if stock already exists
        $is_in_file = false;
        $csv_array = array_map('str_getcsv', file($stocks_file));
        foreach ($csv_array as $key => $value)
        {
            if ($value[0] == $entry)
            {
                $is_in_file = true;
            }
        }

        // Does not exist, so write to file
        if ($is_in_file == false)
        {
            // Generate the CSV line
            $buy_date = date('Y-m-d H:i:s');
            $stock_value = $csv_from_yahoo[0][1];
            $csv_line = array($entry, $shares_amount, $stock_value, $buy_date);

            // Open file for appending
            $handle = fopen("stocks.csv", "a");
            fputcsv($handle, $csv_line); // Append CSV line
            fclose($handle); // Close
            header("Location: welcome.php");
        }
        else
        {
            $error = "* Stock already in portfolio";
        }
    }
    else
    {
        $error = "* Invalid stock symbol or shares amount";
    }
}
?>
<!DOCTYPE html>
<html>
    <head>
        <?php include '../head.php'; ?>
        <script>
            /*
             * Function to clear form
             */
            function clearTextArea()
            {
                document.getElementById('validation_area').innerHTML = "";
            } // End of function
        </script>
    </head>
    <body>
        <div id="perspective" class="perspective effect-airbnb">
            <?php include '../menu.php'; ?>
            <div class="container">
                <div class="wrapper"><!-- wrapper needed for scroll -->
                    <?php
                    $header_title = 'Assignment 4 - Stock Portfolio: Add Stock';
                    include '../header.php';
                    ?>
                    <div id="maintable">
                        <table style="width: 100%;">
                            <td class="maintable_info">
                                <?php include('get_sub_menu.php'); ?>
                                <h3 class="maintable_info_no_hover blog_box_border blog_box_glass">Add stock</h3>
                                <div class="blog_box_with_border">
                                    <br>
                                    <form action="" method="post" name="add_stock_form">
                                        <div style="padding-left: 20px; padding-top: 10px; padding-right: 20px;">
                                            <h4 class="in_form">Stock Symbol *</h4>
                                            <input type="text" name="stock_symbol">
                                            <h4 class="in_form">Shares Amount *</h4> 
                                            <input type="text" name="shares_amount">
                                        </div>
                                        <h4 class="in_form_req_text">(*) Denotes Required Field</h4>
                                        <hr>
                                        <div style="text-align: center;">
                                            <input type="submit" value="Add">
                                            <input type="reset" value="Reset" onclick="clearTextArea();">
                                        </div>
                                        <br>
                                    </form>
                                    <br>
                                </div>
                                <hr>
                                <p class="maintable_info" style="color: red;" id="validation_area">
                                    <?php echo $error; ?>                                    
                                </p>
                                <p id="display_area">
                                </p>
                            </td>
                        </table>
                    </div>
                    <?php include '../footer.php'; ?>
                </div><!-- wrapper -->
            </div><!-- /container -->
            <?php include '../nav.php'; ?>
        </div><!-- /perspective -->
        <script src="../perspective/js/classie.js"></script>
        <script src="../perspective/js/menu.js"></script>
    </body>
</html>