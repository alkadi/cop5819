<?php

function asDollars($value)
{
    return '$' . number_format($value, 2);
}

if (isset($login_session))
{
    // Read the main CSV file of stocks into an array, this is bad if the file is HUGE!
    $csv_array = array_map('str_getcsv', file('stocks.csv'));
    // Total value
    $total = 0.00;
    // Display each record in the proper table
    echo
    "
        <table class='shaded_table' cellspacing='0'>
        <tr>
            <td class='as1'>Stock Symbol</td>
            <td class='as1'>Shares</td>
            <td class='as1'>Value of Stock</td>
            <td class='as1'>Date of Purchase</td>
            <td class='as1'>Total Investment</td>
        </tr>
    ";
    foreach ($csv_array as $key => $value)
    {
        echo
        "
            <tr>
            <td class='as2'>$value[0]</td>
            <td class='as2'>$value[1]</td>
            <td class='as2'>" . asDollars($value[2]) . "</td>
            <td class='as2'>$value[3]</td>
            <td class='as2'>" . asDollars($value[1] * $value[2]) . "</td>
            </tr>
        ";
        $total += ($value[1] * $value[2]);
    }
    echo
    "
        <tr>
        <td colspan='4' class='as1'>Total Value</td>
        <td class='as1'>" . asDollars($total) . "</td>
        </tr>
    ";
    echo "</table>";
}
else
{
    // you're not logged in
    header("Location: index.php");
}
?>