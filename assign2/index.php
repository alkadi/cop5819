<!DOCTYPE html>

<html>
    <head>
        <?php include '../head.php'; ?>        
        <script>
            /*
             * Function to clear form
             */
            function clearTextArea()
            {
                document.getElementById('display_area').innerHTML = "";
                document.getElementById('validation_area').innerHTML = "";
            } // End of function

            /*
             * Function to validate form
             */
            function validateForm(form)
            {
                // Variables
                var is_error = false;
                var error_msg = "Form Errors:<br>";

                // Validate first name
                var x = document.forms["degree_form"]["firstname"].value;
                if (x === null || x === "" || !isNaN(x))
                {
                    error_msg += " * First Name Error, must be provided with no numbers<br>";
                    is_error = true;
                }

                // Validate last name
                x = document.forms["degree_form"]["lastname"].value;
                if (x === null || x === "" || !isNaN(x))
                {
                    error_msg += " * Last Name Error, must be provided with no numbers<br>";
                    is_error = true;
                }

                // Validate at least one check box used
                if (form.CB_EE.checked === false
                        && form.CB_ME.checked === false
                        && form.CB_CE.checked === false
                        && form.CB_CS.checked === false)
                {
                    error_msg += " * Select at least one check box<br>";
                    is_error = true;
                }

                if (is_error === true)
                {
                    document.getElementById('validation_area').innerHTML = error_msg;
                }
                else
                {
                    document.getElementById('validation_area').innerHTML = "";
                    displayResults(form);
                }
            } // End of function

            /*
             * Function to display the results into form
             */
            function displayResults(form)
            {
                var display_body = "<h3 class='maintable_info_no_hover blog_box_border blog_box_glass'>UNF's CCEC Degrees Report for ";
                display_body += document.forms["degree_form"]["firstname"].value
                        + " "
                        + document.forms["degree_form"]["lastname"].value
                        + "</h3>";
                display_body += "<div class='blog_box_with_border'>";
                if (form.CB_EE.checked === true)
                {
                    display_body += "<h3 class='maintable_info_no_hover'>Electrical Engineering</h3>";
                    display_body += "<p class='maintable_info'>Electrical engineers harness \n\
                    electrical energy for the benefit of humankind. The profession is broad and \n\
                    encompasses products valued by society in many technical areas from communications \n\
                    to electric power and energy use to those for our current 'Information Age'. \n\
                    Employment opportunities range over product design, development, manufacturing, \n\
                    sales, management, teaching, and research. Employers include industrial companies, \n\
                    consulting firms, and government agencies and non-governmental organizations. \n\
                    The UNF Office of Career Services provides information on companies seeking \n\
                    electrical engineering graduates for permanent positions or current students \n\
                    for employment in cooperative education positions.</p>";
                }
                if (form.CB_ME.checked === true)
                {
                    display_body += "<h3 class='maintable_info_no_hover'>Mechanical Engineering</h3>";
                    display_body += "<p class='maintable_info'>Mechanical engineering is concerned \n\
                    with energy and its transformations and the design of objects and structures that move. Mechanical \n\
                    engineers are responsible for conceiving, designing, manufacturing, testing, and marketing devices and systems \n\
                    that alter, transfer, transform and utilize the energy form that ultimately causes motion. Employment opportunities \n\
                    range over product design, development, manufacturing, sales, management, teaching, and research. Employers \n\
                    include industrial companies, consulting firms, and government agencies and non-governmental organizations. \n\
                    The UNF Office of Career Services provides information on companies seeking mechanical engineering \n\
                    graduates for permanent positions or current students for employment in \n\
                    cooperative education positions.</p>";
                }
                if (form.CB_CE.checked === true)
                {
                    display_body += "<h3 class='maintable_info_no_hover'>Civil Engineering</h3>";
                    display_body += "<p class='maintable_info'>Civil engineers design the built environment\n\
                    - the structures, roads, water supply systems, and much more - that surrounds us. The profession \n\
                    is broad and encompasses several technical areas including structures, transportation, \n\
                    geotechnics, water resources and environmental protection. Employment opportunities are \n\
                    plentiful in design, construction, management, teaching, and research. Employers include \n\
                    consulting firms, industrial companies, and government agencies and non-governmental \n\
                    organizations. The UNF Office of Career Services provides information on companies \n\
                    seeking civil engineering graduates for permanent positions or current students \n\
                    for employment in cooperative education positions.</p>";
                }
                if (form.CB_CS.checked === true)
                {
                    display_body += "<h3 class='maintable_info_no_hover'>Computer Science</h3>";
                    display_body += "<p class='maintable_info'>The School of Computing \n\
                    is dedicated to the promotion of an academically exciting and progressive \n\
                    intellectual climate, characterized by a superior program of instruction, peer-recognized \n\
                    scholarship, effective support services, and productive professional community involvement. \n\
                    In particular, the School is committed to offering undergraduate and graduate \n\
                    degree programs observing national standards, maintaining and expanding course \n\
                    offerings to keep pace with the rapid development of computer theory and computer technology.</p>";
                }
                if (form.DD_tuition.value === "instate")
                {
                    display_body += "<h3 class='maintable_info_no_hover'>Cost of Education Based on Location</h3>";
                    display_body += "<p class='maintable_info'>$174.12 per credit hour for FL residents</p>";
                }
                else
                {
                    display_body += "<h3 class='maintable_info_no_hover'>Cost of Education Based on Location</h3>";
                    display_body += "<p class='maintable_info'>$654.25 per credit hour for non FL residents</p>";
                }
                display_body += "</div><hr>";
                document.getElementById('display_area').innerHTML = display_body;
            }
        </script>
    </head>
    <body>
        <div id="perspective" class="perspective effect-airbnb">
            <?php include '../menu.php'; ?>
            <div class="container">
                <div class="wrapper"><!-- wrapper needed for scroll -->
                    <?php
                    $header_title = 'Assignment 2 - Web Forms and Validation';
                    include '../header.php';
                    ?>                    
                    <div id="maintable">
                        <table style="width: 100%;">
                            <td class="maintable_info">
                                <h3 class="maintable_info_no_hover blog_box_border blog_box_glass">UNF's CCEC Degree Info. Center</h3>
                                <div class="blog_box_with_border">
                                    <br>
                                    <form name="degree_form" onsubmit="validateForm(this);
                                            return false;">
                                        <div style="padding-left: 20px; padding-top: 10px; padding-right: 20px;">
                                            <h4 class="in_form">First Name *</h4>
                                            <input type="text" name="firstname">
                                            <h4 class="in_form">Last Name *</h4> 
                                            <input type="text" name="lastname">
                                            <h4 class="in_form">Gender *</h4> 
                                            <input type="radio" name="sex" value="male" checked>Male
                                            <input type="radio" name="sex" value="female">Female
                                            <h4 class="in_form">What degree program(s) are you interested in? (Select at least one)</h4>
                                            <table style="margin-top: 0; margin-bottom: 0;">
                                                <tr>
                                                    <td><input type="checkbox" name="CB_EE" value="EE">Electrical Engineering</td>
                                                    <td><input type="checkbox" name="CB_ME" value="ME">Mechanical Engineering</td>
                                                </tr>
                                                <tr>
                                                    <td><input type="checkbox" name="CB_CE" value="CE">Civil Engineering</td>
                                                    <td><input type="checkbox" name="CB_CS" value="CS">Computer Science</td>
                                                </tr>
                                            </table>
                                            <h4 class="in_form">Do you live in Florida?</h4>
                                            <select name="DD_tuition">
                                                <option value="instate">Yes</option>
                                                <option value="outofstate">No</option>
                                            </select>   
                                        </div>
                                        <h4 class="in_form_req_text">(*) Denotes Required Field</h4>
                                        <hr>
                                        <div style="text-align: center;">
                                            <input type="submit" value="Submit">
                                            <input type="reset" value="Reset" onclick="clearTextArea();">
                                        </div>
                                        <br>
                                    </form>
                                    <br>
                                    <a class="maintable_info" href="assign2_flowchart.png" target="_blank">(Click here for Flow Chart)</a>
                                    <br>
                                    <br>
                                </div>
                                <hr>
                                <p class="maintable_info" style="color: red;" id="validation_area">
                                </p>
                                <p id="display_area">
                                </p>
                            </td>
                        </table>
                    </div>
                    <?php include '../footer.php'; ?>
                </div><!-- wrapper -->
            </div><!-- /container -->
            <?php include '../nav.php'; ?>
        </div><!-- /perspective -->
        <script src="../perspective/js/classie.js"></script>
        <script src="../perspective/js/menu.js"></script>
    </body>
</html>