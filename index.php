<!DOCTYPE html>

<html>
    <head>
        <?php include 'head.php'; ?>
    </head>
    <body>
        <div id="perspective" class="perspective effect-airbnb">
            <?php include 'menu.php'; ?>
            <div class="container">
                <div class="wrapper"><!-- wrapper needed for scroll -->
                    <?php
                    $header_title = 'E-Portfolio for COP5819: Internet Programming';
                    include './header.php';
                    ?>
                    <div id="maintable">
                        <table style="width: 100%;">
                            <td class="maintable_info">
                                <h3 class="maintable_info_no_hover blog_box_border blog_box_glass">Welcome!</h3>
                                <div class="blog_box_with_border">
                                    <p class="maintable_info">
                                        In this portfolio I will be tracking my progress through UNF's Internet Programming class!
                                        I will be updating this listing as the class goes on to explain what I'm learning
                                    </p>
                                    <?php include 'rss_feed.php'; ?>
                                    <br>
                                </div>
                                <hr>
                                <a class="big_links" href="./assign1/index.php">
                                    <h3 class="big_links maintable_info blog_box_border blog_box_glass">Assignment 1</h3>
                                </a>
                                <div class="blog_box_with_border">
                                    <p class="maintable_info">
                                        In this assignment I will be trying out simple HTML and CSS styling to create a simple 
                                        'About Me' page. Some of the concepts used will be:
                                    </p>
                                    <ul class="maintable_info">
                                        <li>HTML formatting</li>
                                        <li>CSS3 formatting</li>
                                        <li>Linking pages</li>                                
                                    </ul>   
                                </div>
                                <hr>
                                <a class="big_links" href="./assign2/index.php">
                                    <h3 class="big_links maintable_info_no_hover blog_box_border blog_box_glass">Assignment 2</h3>
                                </a>                        
                                <div class="blog_box_with_border">
                                    <p class="maintable_info">
                                        In this assignment I will be experimenting with Web Forms, a simple UNF CCEC Degree Info.
                                        Center was created to pull information about a set of specific degrees requested by the user.
                                        I covered the following:
                                    </p>
                                    <ul class="maintable_info">
                                        <li>Text fields with validation</li>
                                        <li>Radio buttons</li>
                                        <li>Check boxes and validation</li>     
                                        <li>Drop Down Lists</li>
                                    </ul>   
                                </div>
                                <hr>
                                <a class="big_links" href="./assign3/index.php">
                                    <h3 class="big_links maintable_info_no_hover blog_box_border blog_box_glass">Assignment 3</h3>
                                </a>                        
                                <div class="blog_box_with_border">
                                    <p class="maintable_info">
                                        In this assignment a Java Script library was written and used to calculate the standard 
                                        statistics of a given set. The assignment also involves a text area for data input. RegEx
                                        was used to parse the user's input from the text area, the data was then broken into an array 
                                        of integers and processed accordingly. Although validation was not required, it is still 
                                        performed robustly. The following concepts were covered:
                                    </p>
                                    <ul class="maintable_info">
                                        <li>Text area with validation via RegEx</li>
                                        <li>Reset and submit buttons and attached functions</li>
                                        <li>Linking, writing and using a Java Script library</li>
                                        <li>Performing mathematical functions in Java Script</li>
                                        <li>Handling form input errors and correcting them</li>
                                        <li>Gradient and shading effects using CSS</li>
                                        <li>Simple algorithm design, for Mode function</li>
                                        <li>Mode function supports multiple modes via smart string return</li>
                                        <li>PHP was used for modular page design</li>
                                        <li>Overall 3D look and feel of site was achieved using CSS</li>
                                    </ul>
                                </div>
                                <hr>
                                <a class="big_links" href="./assign4/index.php">
                                    <h3 class="big_links maintable_info_no_hover blog_box_border blog_box_glass">Assignment 4</h3>
                                </a>                        
                                <div class="blog_box_with_border">
                                    <p class="maintable_info">
                                        In this project I will explore the PHP language. A simple stocks manager application 
                                        was written that allows the user to login using a session, add stocks to their 
                                        portfolio and then delete them or modify them. All pages will be protected by the login. 
                                        MySQL will be used to store the user's information for login purposes. Some of the 
                                        concepts used are as follows:
                                    </p>
                                    <ul class="maintable_info">
                                        <li>Form validation using PHP</li>
                                        <li>Validating username and password through MySQL</li>
                                        <li>PHP sessions and session variables</li>
                                        <li>Accessing files through PHP</li>
                                        <li>Writing to file using PHP</li>
                                        <li>Modifying a file using PHP</li>
                                        <li>Dealing with maps and arrays to parse CSV</li>
                                        <li>Interfacing to Yahoo Finance API to retrieve stocks information</li>
                                        <li>Logout feature with session destroy using PHP</li>
                                        <li>Formatting tables using PHP functions</li>
                                    </ul>
                                </div>
                                <hr>
                                <a class="big_links" href="./assign5/index.php">
                                    <h3 class="big_links maintable_info_no_hover blog_box_border blog_box_glass">Assignment 5</h3>
                                </a>                        
                                <div class="blog_box_with_border">
                                    <p class="maintable_info">
                                        In this project I will be doing basically what Project 4 did but with MySQL instead 
                                        of writing to a CSV file. Login information is the same as project 4. Some of the 
                                        concepts used are as follows:
                                    </p>
                                    <ul class="maintable_info">
                                        <li>Creating tables with MySQL</li>
                                        <li>Updating and editing tables with MySQL</li>
                                        <li>Manipulating MySQL with PHP</li>
                                        <li>Handling sessions and session variables</li>
                                        <li>Protect against MySQL injections using the addslashes() PHP function</li>
                                        <li>Basic ERD generation and utilization</li>
                                        <li>Sharing PHP scripts between projects</li>
                                    </ul>
                                </div>
                                <hr>
                                <a class="big_links" href="./assign6/index.php">
                                    <h3 class="big_links maintable_info_no_hover blog_box_border blog_box_glass">Assignment 6</h3>
                                </a>                        
                                <div class="blog_box_with_border">
                                    <p class="maintable_info">
                                        In this project I will be using the data from the previous assignment to demo how 
                                        data can be displayed without refreshing the page using AJAX. MySQL DB will be used 
                                        to access the data exactly like the previous project. Some of the concepts used 
                                        are as follows:
                                    </p>
                                    <ul class="maintable_info">
                                        <li>AJAX page manipulation</li>
                                        <li>Support different browsers</li>
                                        <li>Using lists in AJAX</li>
                                        <li>Passing variables to a stand-alone PHP script using the URL</li>
                                    </ul>
                                </div>
                                <hr>
                            </td>
                        </table>
                    </div>
                    <?php include './footer.php'; ?>
                </div><!-- wrapper -->
            </div><!-- /container -->
            <?php include 'nav.php'; ?>
        </div><!-- /perspective -->
        <script src="perspective/js/classie.js"></script>
        <script src="perspective/js/menu.js"></script>
    </body>
</html>