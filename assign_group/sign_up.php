<?php
include('../assign4/config.php');
session_start();

$error = "";
$is_error = false;

// Functions
function javaAlert($msg)
{
    echo '<script language="javascript">';
    echo 'alert("' . $msg . '")';
    echo '</script>';
}

function reArrayFiles(&$file_post)
{
    $file_ary = array();
    $file_count = count($file_post['name']);
    $file_keys = array_keys($file_post);

    for ($i = 0; $i < $file_count; $i++)
    {
        foreach ($file_keys as $key)
        {
            $file_ary[$i][$key] = $file_post[$key][$i];
        }
    }
    return $file_ary;
}

function echoCheckedIfTrue($checkbox_value)
{
    if ($checkbox_value)
    {
        echo 'checked=""';
    }
    else
    {
        echo '';
    }
}

function generateRandomString($length = 10)
{
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $randomString = '';
    for ($i = 0; $i < $length; $i++)
    {
        $randomString .= $characters[rand(0, strlen($characters) - 1)];
    }
    return $randomString;
}

function verify_save_image(&$file_name)
{
    $allowedExts = array("gif", "jpeg", "jpg", "png");
    $temp = explode(".", $file_name["name"]);
    $extension = end($temp);
    if ((($file_name["type"] == "image/gif")
        || ($file_name["type"] == "image/jpeg")
        || ($file_name["type"] == "image/jpg")
        || ($file_name["type"] == "image/pjpeg" )
        || ($file_name["type"] == "image/x-png" )
        || ($file_name["type"] == "image/png"))
        && ($file_name["size"] < 2000000 ) && in_array($extension, $allowedExts))
    {
        if ($file_name["error"] > 0)
        {
            return "* File Erorr! Return Code: " . $file_name["error"] . "<br>";
        }
        else
        {
            $file_name["name"] = generateRandomString() . "." . $extension;
            while (!file_exists("upload/" . $file_name["name"]))
            {
                $file_name["name"] = generateRandomString() . "." . $extension;
                move_uploaded_file($file_name["tmp_name"], "upload/" . $file_name["name"]);
            }
        }
        return "";
    }
    else
    {
        return "* Invalid or empty picture file.<br>";
    }
}

// Check if we have a variable from session so we can pre-populate the form
if (isset($_SESSION['modify_profile']))
{
    $do_update = true;
    $row = $_SESSION['modify_profile'];
    $id = $row['ID_n'];
    $username = $row['UserName_c'];
    $login_session = $username; // For session handling
    $password = $row['PassWord_c'];
    $first_name = $row['FirstName_c'];
    $last_name = $row['LastName_c'];
    $gender = $row['Gender_c'];
    $email = $row['Email_c'];
    $job_title = $row['JobTitle_c'];
    $profile_pic = $row['ProfilePicFile_c'];
    $sleep_early = (bool) $row['SleepEarly_b'];
    $sleep_late = (bool) $row['SleepLate_b'];
    $smoking = (bool) $row['Smoking_b'];
    $non_smoking = (bool) $row['NonSmoking_b'];
    $many_visitors = (bool) $row['ManyVisitors_b'];
    $not_many_visitors = (bool) $row['NotManyVisitors_b'];
    $alcohol = (bool) $row['Alcohol_b'];
    $no_alcohol = (bool) $row['NoAlcohol_b'];
    $rent_price = $row['RentPrice_n'];
    $bills_amount = $row['BillsAmount_n'];
    $pets_allowed = (bool) $row['PetsAllowed_b'];
    $washer = (bool) $row['Washer_b'];
    $dryer = (bool) $row['Dryer_b'];
    $swimming_pool = (bool) $row['SwimmingPool_b'];
    $hw_floors = (bool) $row['HWFloors_b'];
    $central_ac = (bool) $row['CentralAC_b'];
    $central_heat = (bool) $row['CentralHeat_b'];
    $free_parking = (bool) $row['FreeParking_b'];
    $game_room = (bool) $row['GameRoom_b'];
    $trash_service = (bool) $row['TrashService_b'];
    $pics = $row['Pics_c'];
    $city = $row['City_c'];
    $state = $row['State_c'];
}
else
{
    $do_update = false;
    $id = '';
    $username = '';
    $password = '';
    $first_name = '';
    $last_name = '';
    $gender = 'male';
    $email = '';
    $job_title = '';
    $profile_pic = '';
    $sleep_early = true;
    $sleep_late = false;
    $smoking = false;
    $non_smoking = true;
    $many_visitors = true;
    $not_many_visitors = false;
    $alcohol = true;
    $no_alcohol = false;
    $rent_price = 0.00;
    $bills_amount = 0.00;
    $pets_allowed = false;
    $washer = false;
    $dryer = false;
    $swimming_pool = false;
    $hw_floors = false;
    $central_ac = false;
    $central_heat = false;
    $free_parking = false;
    $game_room = false;
    $trash_service = false;
    $pics = '';
    $city = '';
    $state = '';
}

// Get data from post
if ($_SERVER["REQUEST_METHOD"] == "POST")
{
    // ******* validation *******
    // Username
    if ($do_update == false)
    {
        $username = $_POST['username'];
        if ($username == '' || !preg_match("/^[\w]{4,16}+$/", $username) || $username == null)
        {
            $error.= "* Username Error, must be between 4 and 16 characters and must contain a-z, 0-9 and _ only.<br>";
            $is_error = true;
        }
        else
        {
            // Check to see if user is taken
            $sql = "SELECT ID_n FROM gp_users WHERE UserName_c='" . addslashes($username) . "'";
            $result = mysqli_query($db, $sql);
            $row = mysqli_fetch_array($result, MYSQLI_ASSOC);
            $count = mysqli_num_rows($result);

            if ($count == 1)
            {
                $error.= "* Username Taken!<br>";
                $is_error = true;
            }
        }
    }

    // Password
    $password = $_POST['password'];
    if ($password == '' || !preg_match("/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?!.*\s).{4,16}$/", $password) || $password == null)
    {
        $error.= "* Password Error, must be between 4 and 16 characters and must contain 1 uppercase letter, 1 lowercase letter and 1 number.<br>";
        $is_error = true;
    }

    // First name
    $first_name = trim($_POST['first_name']);
    if ($first_name == '' || ctype_digit($first_name) || $first_name == null)
    {
        $error.= "* First Name Error, must be provided with no numbers.<br>";
        $is_error = true;
    }

    // Last name
    $last_name = trim($_POST['last_name']);
    if ($last_name == '' || ctype_digit($last_name) || $last_name == null)
    {
        $error.= "* Last Name Error, must be provided with no numbers.<br>";
        $is_error = true;
    }

    // Sex
    $gender = $_POST['sex'];

    // Email
    $email = trim($_POST['email']);
    if ($email == '' || !preg_match("/([\w\-]+\@[\w\-]+\.[\w\-]+)/", $email) || $email == null)
    {
        $error.= "* Email Error, Check entered email address.<br>";
        $is_error = true;
    }

    // Job Title, no validation, as is
    $job_title = trim($_POST['job_title']);

    // Profile picture
    if ($do_update == false)
    {
        $cam_pic_data = $_POST['capture_profile_pic'];
        if ($cam_pic_data != null)
        {
            $cam_pic_data = substr($cam_pic_data, 22); // strip header
            $cam_pic_data = base64_decode($cam_pic_data);
            $profile_pic = generateRandomString() . '.png';
            $im = imagecreatefromstring($cam_pic_data);
            if ($im !== false)
            {
                //header('Content-Type: image/png');
                imagepng($im, 'upload/' . $profile_pic);
                imagedestroy($im);
            }
            else
            {
                echo 'An error occurred.';
            }
        }
        else
        {
            if (isset($_FILES['profile_pic']))
            {
                $profile_pic = $_FILES['profile_pic'];
                $image_error = verify_save_image($profile_pic);
                if ($image_error != "")
                {
                    $error.= $image_error;
                    $profile_pic = '';
                    $is_error = true;
                }
                else
                {
                    $profile_pic = $profile_pic['name'];
                }
            }
        }
    }

    // Sleeping habits, sleep_early sleep_late
    if ($_POST['sleep_habit'] == "early")
    {
        $sleep_early = true;
        $sleep_late = false;
    }
    else
    {
        $sleep_early = false;
        $sleep_late = true;
    }

    // Visitors habits, manu, not_many
    if ($_POST['visitors'] == "many")
    {
        $many_visitors = true;
        $not_many_visitors = false;
    }
    else
    {
        $many_visitors = false;
        $not_many_visitors = true;
    }

    // Smoking habits, manu, not_many
    if ($_POST['smoking'] == "yes")
    {
        $smoking = true;
        $non_smoking = false;
    }
    else
    {
        $smoking = false;
        $non_smoking = true;
    }

    // Alcohol habits, yes, no
    if ($_POST['alcohol'] == "yes")
    {
        $alcohol = true;
        $no_alcohol = false;
    }
    else
    {
        $alcohol = false;
        $no_alcohol = true;
    }

    // Rent price
    $rent_price = trim($_POST['rent_price']);
    if ($rent_price < 1 || $rent_price == '' || !ctype_digit($rent_price) || $rent_price == null)
    {
        $error.= "* Rent Price Error, must be provided as an integer greater than 0.<br>";
        $is_error = true;
    }

    // Bills amount, could be 0 so no validation on that
    $bills_amount = trim($_POST['bills_amount']);
    if ($bills_amount == '' || !ctype_digit($bills_amount) || $bills_amount == null)
    {
        $error.= "* Bills Amount Error, must be provided as an integer.<br>";
        $is_error = true;
    }

    // Check boxes
    if (isset($_POST['checkboxes']))
    {
        // Kill all first
        $pets_allowed = false;
        $washer = false;
        $dryer = false;
        $swimming_pool = false;
        $hw_floors = false;
        $central_ac = false;
        $central_heat = false;
        $free_parking = false;
        $game_room = false;
        $trash_service = false;
        // Loop and update
        foreach ($_POST['checkboxes'] as $box)
        {
            switch ($box)
            {
                case 'pets_allowed': $pets_allowed = true;
                    break;
                case 'washer': $washer = true;
                    break;
                case 'dryer': $dryer = true;
                    break;
                case 'swimming_pool': $swimming_pool = true;
                    break;
                case 'hw_floors': $hw_floors = true;
                    break;
                case 'central_ac': $central_ac = true;
                    break;
                case 'central_heat': $central_heat = true;
                    break;
                case 'free_parking': $free_parking = true;
                    break;
                case 'game_room': $game_room = true;
                    break;
                case 'trash_service': $trash_service = true;
                    break;
            }
        }
    }

    // Place pictures
    if ($do_update == false)
    {
        if (isset($_FILES['place_pics']))
        {
            $file_array = reArrayFiles($_FILES['place_pics']);
            foreach ($file_array as $pic)
            {
                $image_error = verify_save_image($pic);
                if ($image_error != "")
                {
                    if ($image_error != '* Invalid or empty picture file.<br>') // ignore emtpy picture
                    {
                        $error.= $image_error;
                        $is_error = true;
                    }
                }
                else
                {
                    if ($image_error != '* Invalid or empty picture file.<br>') // ignore empty picture
                    {
                        $pics.= $pic['name'] . ',';
                    }
                }
            }
            $pics = rtrim($pics, ","); // remove trailing comma
        }
    }

    // City
    $city = trim($_POST['city']);
    if ($city == '' || ctype_digit($city) || $city == null)
    {
        $error.= "* City Error, must be provided with no numbers.<br>";
        $is_error = true;
    }

    // State
    $state = $_POST['DD_states'];
    if ($state == '' || ctype_digit($state) || $state == null)
    {
        $error.= "* State Error, must be provided with no numbers.<br>";
        $is_error = true;
    }

    // only if validation passes
    if ($is_error == false)
    {
        // Create table if doesn't exist
        $sql = "
            CREATE TABLE IF NOT EXISTS `gp_users` (
              `ID_n` int(11) NOT NULL AUTO_INCREMENT,
              `UserName_c` varchar(256) NOT NULL,
              `PassWord_c` varchar(256) NOT NULL,
              `FirstName_c` varchar(256) NOT NULL,
              `LastName_c` varchar(256) NOT NULL,
              `Gender_c` varchar(256) NOT NULL,
              `Email_c` varchar(512) NOT NULL,
              `JobTitle_c` varchar(256) NOT NULL,
              `ProfilePicFile_c` varchar(256) NOT NULL,
              `SleepEarly_b` tinyint(4) NOT NULL,
              `SleepLate_b` tinyint(4) NOT NULL,
              `Smoking_b` tinyint(4) NOT NULL,
              `NonSmoking_b` tinyint(4) NOT NULL,
              `ManyVisitors_b` tinyint(4) NOT NULL,
              `NotManyVisitors_b` tinyint(4) NOT NULL,
              `Alcohol_b` tinyint(4) NOT NULL,
              `NoAlcohol_b` tinyint(4) NOT NULL,
              `RentPrice_n` int(11) NOT NULL,
              `BillsAmount_n` int(11) NOT NULL,
              `PetsAllowed_b` tinyint(4) NOT NULL,
              `Washer_b` tinyint(4) NOT NULL,
              `Dryer_b` tinyint(4) NOT NULL,
              `SwimmingPool_b` tinyint(4) NOT NULL,
              `HWFloors_b` tinyint(4) NOT NULL,
              `CentralAC_b` tinyint(4) NOT NULL,
              `CentralHeat_b` tinyint(4) NOT NULL,
              `FreeParking_b` tinyint(4) NOT NULL,
              `GameRoom_b` tinyint(4) NOT NULL,
              `TrashService_b` tinyint(4) NOT NULL,
              `Pics_c` varchar(1024) NOT NULL,
              `City_c` varchar(512) NOT NULL,
              `State_c` varchar(512) NOT NULL,
              PRIMARY KEY (`ID_n`),
              UNIQUE KEY `ID_n` (`ID_n`)
            ) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;
        ";
        if (!mysqli_query($db, $sql))
        {
            javaAlert('Table creation error!');
        }

        if ($do_update == true)
        {
            $sql = "
                UPDATE `gp_users` SET
                UserName_c = '" . addslashes($username) . "', 
                PassWord_c = '" . addslashes($password) . "', 
                FirstName_c = '" . addslashes($first_name) . "', 
                LastName_c = '" . addslashes($last_name) . "', 
                Gender_c = '" . addslashes($gender) . "', 
                Email_c = '" . addslashes($email) . "', 
                JobTitle_c = '" . addslashes($job_title) . "', 
                SleepEarly_b = '" . addslashes($sleep_early) . "', 
                SleepLate_b = '" . addslashes($sleep_late) . "', 
                Smoking_b = '" . addslashes($smoking) . "', 
                NonSmoking_b = '" . addslashes($non_smoking) . "', 
                ManyVisitors_b = '" . addslashes($many_visitors) . "', 
                NotManyVisitors_b = '" . addslashes($not_many_visitors) . "', 
                Alcohol_b = '" . addslashes($alcohol) . "', 
                NoAlcohol_b = '" . addslashes($no_alcohol) . "', 
                RentPrice_n = '" . addslashes($rent_price) . "', 
                BillsAmount_n = '" . addslashes($bills_amount) . "', 
                PetsAllowed_b = '" . addslashes($pets_allowed) . "', 
                Washer_b = '" . addslashes($washer) . "', 
                Dryer_b = '" . addslashes($dryer) . "', 
                SwimmingPool_b = '" . addslashes($swimming_pool) . "', 
                HWFloors_b = '" . addslashes($hw_floors) . "', 
                CentralAC_b = '" . addslashes($central_ac) . "', 
                CentralHeat_b = '" . addslashes($central_heat) . "', 
                FreeParking_b = '" . addslashes($free_parking) . "', 
                GameRoom_b = '" . addslashes($game_room) . "', 
                TrashService_b = '" . addslashes($trash_service) . "', 
                GameRoom_b = '" . addslashes($game_room) . "', 
                City_c = '" . addslashes($city) . "', 
                State_c = '" . addslashes($state) . "'
                WHERE ID_n='" . addslashes($id) . "'
            ";
        }
        else
        {
            $sql = " 
                INSERT INTO `gp_users` 
                (
                    `UserName_c`, `PassWord_c`, `FirstName_c`, `LastName_c`, `Gender_c`, 
                    `Email_c`, `JobTitle_c`, `ProfilePicFile_c`, `SleepEarly_b`, `SleepLate_b`, 
                    `Smoking_b`, `NonSmoking_b`, `ManyVisitors_b`, `NotManyVisitors_b`, `Alcohol_b`, `NoAlcohol_b`, 
                    `RentPrice_n`, `BillsAmount_n`, `PetsAllowed_b`, `Washer_b`, `Dryer_b`, `SwimmingPool_b`, 
                    `HWFloors_b`, `CentralAC_b`, `CentralHeat_b`, `FreeParking_b`, `GameRoom_b`, 
                    `TrashService_b`, `Pics_c`, `City_c`, `State_c`
                )
                VALUES 
                (
                    '" . addslashes($username) . "', 
                    '" . addslashes($password) . "', 
                    '" . addslashes($first_name) . "', 
                    '" . addslashes($last_name) . "', 
                    '" . addslashes($gender) . "',
                    '" . addslashes($email) . "',
                    '" . addslashes($job_title) . "',
                    '" . addslashes($profile_pic) . "',
                    '" . addslashes($sleep_early) . "',
                    '" . addslashes($sleep_late) . "',
                    '" . addslashes($smoking) . "',
                    '" . addslashes($non_smoking) . "',
                    '" . addslashes($many_visitors) . "',
                    '" . addslashes($not_many_visitors) . "',
                    '" . addslashes($alcohol) . "',
                    '" . addslashes($no_alcohol) . "',
                    '" . addslashes($rent_price) . "',
                    '" . addslashes($bills_amount) . "',
                    '" . addslashes($pets_allowed) . "',
                    '" . addslashes($washer) . "',
                    '" . addslashes($dryer) . "',
                    '" . addslashes($swimming_pool) . "',
                    '" . addslashes($hw_floors) . "',
                    '" . addslashes($central_ac) . "',
                    '" . addslashes($central_heat) . "',
                    '" . addslashes($free_parking) . "',
                    '" . addslashes($game_room) . "',
                    '" . addslashes($trash_service) . "',
                    '" . addslashes($pics) . "',
                    '" . addslashes($city) . "',
                    '" . addslashes($state) . "'
                );
            ";
        }

        if (!mysqli_query($db, $sql))
        {
            javaAlert('DB error or username taken!');
            // clean up
            // delete profile pic
            unlink('upload/' . $profile_pic);
            // delete all pics
            $pic_files = explode(',', $pics);
            if (isset($pics) && $pics != null && $pics != '')
            {
                foreach ($pic_files as &$pic)
                {
                    unlink('upload/' . $pic);
                }
            }
        }
        else
        {
            // We are done
            if (isset($_SESSION ['modify_profile']))
            {
                unset($_SESSION['modify_profile']);
            }
            header('Location: welcome.php');
        }
    }
    else
    {
        // clean up
        // delete profile pic
        if (isset($profile_pic) && $profile_pic != null && $profile_pic != '')
        {
            unlink('upload/' . $profile_pic);
        }
        // delete all pics
        if (isset($pics) && $pics != null && $pics != '')
        {
            $pic_files = explode(',', $pics);
            foreach ($pic_files as &$pic)
            {
                unlink('upload/' . $pic);
            }
        }
    }
}
?>
<!DOCTYPE html>
<html>
    <head>
        <?php include '../head.php'; ?>
        <script>
            /*
             * Function to clear form
             */
            function clearTextArea()
            {
                document.getElementById('validation_area').innerHTML = "";
            } // End of function
            /*
             * Function to capture web cam
             */
            function getCamCapture()
            {
                jq.fancybox({
                    type: 'ajax',
                    href: 'camera_interface.php',
                    afterShow: function() {
                        captureVideo();
                    },
                    afterClose: function() {
                        // Stop webcame
                        window.stream.stop();
                    }
                });
            }
            /*
             * Get image from web camera
             */
            function getImage()
            {
                var imageData = jq("#webcam")[0].toDataURL();
                jq('#camContainer').hide();
                jq('#camPreview').show().find('img').attr('src', imageData).width(300);
                var imageData = jq("#webcam")[0].toDataURL();
                jq('#capture_profile_pic').val(imageData);
            }

            function saveCamCapture() {
                jq('#capture_msg').text("(Image captured from web-cam!)");
                jq.fancybox.close();
            }
            function retakeCamCapture()
            {
                jq('#camContainer').show();
                jq('#camPreview').hide();
            }
        </script>
    </head>
    <body>
        <div id="perspective" class="perspective effect-airbnb">
            <?php include '../menu.php'; ?>
            <div class="container">
                <div class="wrapper"><!-- wrapper needed for scroll -->
                    <?php
                    $header_title = 'Group Project - Roomie Track™: Sign Up or Update Profile';
                    include '../header.php';
                    ?>
                    <div id="maintable">
                        <table style="width: 100%;">
                            <td class="maintable_info">
                                <?php
                                if ($do_update == true)
                                {
                                    include('get_sub_menu.php');
                                }
                                else
                                {
                                    include('get_main_app_menu.php');
                                }
                                ?>
                                <h3 class="maintable_info_no_hover blog_box_border blog_box_glass">Sign up or update profile</h3>
                                <div class="blog_box_with_border">
                                    <br>
                                    <form action="" method="post" name="sign_up_form" enctype="multipart/form-data">
                                        <div style="padding-left: 20px; padding-top: 10px; padding-right: 20px;">
                                            <!-- username -->
                                            <?php
                                            if ($do_update == false)
                                            {
                                                echo '<h4 class="in_form">Username *</h4>';
                                                echo "<input type='text' name='username' value='$username'>";
                                            }
                                            ?>
                                            <!-- password -->
                                            <h4 class="in_form">Password *</h4>
                                            <input type="password" name="password" value="<?php echo ($password); ?>">
                                            <!-- first name -->
                                            <h4 class="in_form">First Name *</h4>
                                            <input type="text" name="first_name" value="<?php echo ($first_name); ?>">
                                            <!-- last name -->
                                            <h4 class="in_form">Last Name *</h4> 
                                            <input type="text" name="last_name" value="<?php echo ($last_name); ?>">
                                            <!-- gender -->
                                            <h4 class="in_form">Gender *</h4> 
                                            <?php
                                            if ($gender == 'male')
                                            {
                                                echo '<input type="radio" name="sex" value="male" checked>Male';
                                                echo '<input type="radio" name="sex" value="female">Female';
                                            }
                                            else
                                            {
                                                echo '<input type="radio" name="sex" value="male">Male';
                                                echo '<input type="radio" name="sex" value="female" checked>Female';
                                            }
                                            ?>
                                            <!-- email -->
                                            <h4 class="in_form">Email *</h4>
                                            <input type="text" name="email" value="<?php echo ($email); ?>">
                                            <!-- job title -->
                                            <h4 class="in_form">Job Title</h4> 
                                            <input type="text" name="job_title" value="<?php echo ($job_title); ?>">
                                            <!-- profile pic -->
                                            <?php
                                            if ($do_update == false)
                                            {
                                                echo '<h4 class="in_form">Profile Picture *</h4>';
                                                echo '<input id="capture_profile_pic" type="hidden" name="capture_profile_pic"></input>';
                                                echo '<input type="button" value="Capture from camera" onclick="getCamCapture();"></input>';
                                                echo '<input type="file" name="profile_pic" accept="image/*">';
                                                echo '<span id="capture_msg"></span>';
                                            }
                                            ?>
                                            <!-- sleep habit -->
                                            <h4 class="in_form">Sleeping Habits *</h4> 
                                            <?php
                                            if ($sleep_early == true)
                                            {
                                                echo '<input type="radio" name="sleep_habit" value="early" checked>Sleep early';
                                                echo '<input type="radio" name="sleep_habit" value="late">Sleep late';
                                            }
                                            else
                                            {
                                                echo '<input type="radio" name="sleep_habit" value="early">Sleep early';
                                                echo '<input type="radio" name="sleep_habit" value="late" checked>Sleep late';
                                            }
                                            ?>
                                            <!-- visitors habit -->
                                            <h4 class="in_form">Visitors Habits *</h4> 
                                            <?php
                                            if ($many_visitors == true)
                                            {
                                                echo '<input type="radio" name="visitors" value="many" checked>Many';
                                                echo '<input type="radio" name="visitors" value="not_many">Not many';
                                            }
                                            else
                                            {
                                                echo '<input type="radio" name="visitors" value="many">Many';
                                                echo '<input type="radio" name="visitors" value="not_many" checked>Not many';
                                            }
                                            ?>
                                            <!-- smoking habits -->
                                            <h4 class="in_form">Smoking Habits *</h4> 
                                            <?php
                                            if ($smoking == true)
                                            {
                                                echo '<input type="radio" name="smoking" value="yes" checked>Yes';
                                                echo '<input type="radio" name="smoking" value="no">No';
                                            }
                                            else
                                            {
                                                echo '<input type="radio" name="smoking" value="yes">Yes';
                                                echo '<input type="radio" name="smoking" value="no" checked>No';
                                            }
                                            ?>
                                            <!-- alcohol habits -->
                                            <h4 class="in_form">Alcohol Habits *</h4> 
                                            <?php
                                            if ($alcohol == true)
                                            {
                                                echo '<input type="radio" name="alcohol" value="yes" checked>Alcohol allowed';
                                                echo '<input type="radio" name="alcohol" value="no">Alcohol not allowed';
                                            }
                                            else
                                            {
                                                echo '<input type="radio" name="alcohol" value="yes">Alcohol allowed';
                                                echo '<input type="radio" name="alcohol" value="no" checked>Alcohol not allowed';
                                            }
                                            ?>
                                            <!-- rent price -->
                                            <h4 class="in_form">Rent Price *</h4>
                                            <input type="text" name="rent_price" value="<?php echo ($rent_price); ?>">
                                            <!-- bills amount -->
                                            <h4 class="in_form">Bills amount</h4>
                                            <input type="text" name="bills_amount" value="<?php echo ($bills_amount); ?>">
                                            <!-- check boxes -->
                                            <h4 class="in_form">Select all that applies</h4>
                                            <table style="margin-top: 0; margin-bottom: 0;">
                                                <tr>
                                                    <td><input type="checkbox" name="checkboxes[]" value="pets_allowed" <?php echoCheckedIfTrue($pets_allowed); ?>>Pets allowed</td>
                                                    <td><input type="checkbox" name="checkboxes[]" value="washer" <?php echoCheckedIfTrue($washer); ?>>Washer</td>
                                                    <td><input type="checkbox" name="checkboxes[]" value="dryer" <?php echoCheckedIfTrue($dryer); ?>>Dryer</td>
                                                    <td><input type="checkbox" name="checkboxes[]" value="swimming_pool" <?php echoCheckedIfTrue($swimming_pool); ?>>Swimming pool</td>
                                                    <td><input type="checkbox" name="checkboxes[]" value="hw_floors" <?php echoCheckedIfTrue($hw_floors); ?>>Hard wood floors</td>
                                                </tr>
                                                <tr>
                                                    <td><input type="checkbox" name="checkboxes[]" value="central_ac" <?php echoCheckedIfTrue($central_ac); ?>>Central AC</td>
                                                    <td><input type="checkbox" name="checkboxes[]" value="central_heat" <?php echoCheckedIfTrue($central_heat); ?>>Central heating</td>
                                                    <td><input type="checkbox" name="checkboxes[]" value="free_parking" <?php echoCheckedIfTrue($free_parking); ?>>Free parking</td>
                                                    <td><input type="checkbox" name="checkboxes[]" value="game_room" <?php echoCheckedIfTrue($game_room); ?>>Game room</td>
                                                    <td><input type="checkbox" name="checkboxes[]" value="trash_service" <?php echoCheckedIfTrue($trash_service); ?>>Trash service</td>
                                                </tr>
                                            </table>
                                            <!-- place pictures -->
                                            <?php
                                            if ($do_update == false)
                                            {
                                                echo '<h4 class="in_form">Place Pictures</h4>';
                                                echo '<input type="file" multiple="multiple" name="place_pics[]" accept="image/*">';
                                                echo '<input type="file" multiple="multiple" name="place_pics[]" accept="image/*">';
                                                echo '<input type="file" multiple="multiple" name="place_pics[]" accept="image/*">';
                                                echo '<input type="file" multiple="multiple" name="place_pics[]" accept="image/*">';
                                            }
                                            ?>
                                            <!-- city -->
                                            <h4 class="in_form">City *</h4>
                                            <input type="text" name="city" value="<?php echo $city ?>">
                                            <!-- state -->
                                            <h4 class="in_form">State *</h4>
                                            <?php include('get_states_list.php'); ?>
                                            <script>
                                                var myvar = <?php echo json_encode($state); ?>;
                                                for (var i = 0; i < document.getElementById("DD_states").length; i++)
                                                {
                                                    if (document.getElementById("DD_states").options[i].value === myvar)
                                                    {
                                                        document.getElementById("DD_states").selectedIndex = i;
                                                    }
                                                }
                                            </script>
                                        </div>
                                        <h4 class="in_form_req_text">(*) Denotes Required Field</h4>
                                        <hr>
                                        <div style="text-align: center;">
                                            <?php
                                            echo($do_update ? '<input type="submit" value="Update">' : '<input type="submit" value="Sign Up">');
                                            ?>
                                            <input type="reset" value="Reset" onclick="clearTextArea();">
                                        </div>
                                        <br>
                                    </form>
                                    <br>
                                </div>
                                <hr>
                                <p class="maintable_info" style="color: red;" id="validation_area">
                                    <?php echo $error; ?>                                    
                                </p>
                                <p id="display_area">
                                </p>
                            </td>
                        </table>
                    </div>
                    <?php include '../footer.php'; ?>
                </div><!-- wrapper -->
            </div><!-- /container -->
            <?php include '../nav.php'; ?> 
        </div><!-- /perspective -->
        <script src="../perspective/js/classie.js"></script>
        <script  src="../perspective/js/menu.js"></script>
    </body>
</html>