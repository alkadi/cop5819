<div id="camcapture">
    <div id="camContainer">
        <video autoplay="" style="display:none;"></video>
        <canvas id="webcam"> 
        </canvas>
        <div style="text-align: center;">
            <input type="submit" value="Snap" onclick="getImage();">
        </div>
    </div>
    <div id="camPreview" style="display:none;">
        <img />
        <div style="text-align: center;">
            <input type="button" value="Ok" onclick="saveCamCapture();"></input>
            <input type="button" value="Retake" onclick="retakeCamCapture();"></input>
        </div>
    </div>
    <script src="../assignx/video-capture.js"></script>
</div>