<?php
include('lock.php');

// Functions
function javaAlert($msg)
{
    echo '<script language="javascript">';
    echo 'alert("' . $msg . '")';
    echo '</script>';
}

// Let's get all the user's info
$sql = "SELECT * FROM `gp_users` WHERE `UserName_c` = '" . $login_session . "';";
$result = mysqli_query($db, $sql);
$user_data = mysqli_fetch_array($result, MYSQLI_ASSOC);

if ($_SERVER["REQUEST_METHOD"] == "POST")
{
    $pictures = explode(',', $user_data['Pics_c']);
    $profile_pic = $user_data['ProfilePicFile_c'];
    // delete profile pic
    unlink('upload/' . $profile_pic);
    // delete all pics
    foreach ($pictures as &$pic)
    {
        unlink('upload/' . $pic);
    }
    // Now delete the user from the DB
    $id_to_delete = $user_data['ID_n'];
    $sql = "DELETE FROM `gp_users` WHERE `ID_n` = $id_to_delete";
    if (mysqli_query($db, $sql))
    {
        session_destroy();
        header("Location: index.php");
    }
    else
    {
        javaAlert("Database Error!");
    }
}
?>
<!DOCTYPE html>
<html>
    <head>
        <?php include '../head.php'; ?>
    </head>
    <body>
        <div id="perspective" class="perspective effect-airbnb">
            <?php include '../menu.php'; ?>
            <div class="container">
                <div class="wrapper"><!-- wrapper needed for scroll -->
                    <?php
                    $header_title = 'Group Project - Roomie Track™: Delete Profile';
                    include '../header.php';
                    ?>
                    <div id="maintable">
                        <table style="width: 100%;">
                            <td class="maintable_info">
                                <!-- sub menu -->
                                <?php include('get_sub_menu.php'); ?>
                                <!-- delete profile -->
                                <h3 class="maintable_info_no_hover blog_box_border blog_box_glass">Delete profile</h3>
                                <div class="blog_box_with_border">
                                    <br>
                                    <form action="" method="post" name="delete_entry_form">
                                        <div style="padding-left: 20px; padding-top: 10px; padding-right: 20px;">
                                            <h4 class="in_form" style="text-align: center;">Are you sure you want to delete your profile?</h4>
                                        </div>
                                        <h4 class="in_form_req_text"><!--(*) Denotes Required Field--></h4>
                                        <hr>
                                        <div style="text-align: center;">
                                            <input type="submit" value="Yes Delete">
                                        </div>
                                        <br>
                                    </form>
                                    <br>
                                </div>
                                <hr>
                            </td>
                        </table>
                    </div>
                    <?php include '../footer.php'; ?>
                </div><!-- wrapper -->
            </div><!-- /container -->
            <?php include '../nav.php'; ?>
        </div><!-- /perspective -->
        <script src="../perspective/js/classie.js"></script>
        <script src="../perspective/js/menu.js"></script>
    </body>
</html>