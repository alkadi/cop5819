<?php
include('lock.php');
$error = "";
$is_error = false;

// Functions
function javaAlert($msg)
{
    echo '<script language="javascript">';
    echo 'alert("' . $msg . '")';
    echo '</script>';
}

function reArrayFiles(&$file_post)
{
    $file_ary = array();
    $file_count = count($file_post['name']);
    $file_keys = array_keys($file_post);

    for ($i = 0; $i < $file_count; $i++)
    {
        foreach ($file_keys as $key)
        {
            $file_ary[$i][$key] = $file_post[$key][$i];
        }
    }
    return $file_ary;
}

function generateRandomString($length = 10)
{
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $randomString = '';
    for ($i = 0; $i < $length; $i++)
    {
        $randomString .= $characters[rand(0, strlen($characters) - 1)];
    }
    return $randomString;
}

function verify_save_image(&$file_name)
{
    $allowedExts = array("gif", "jpeg", "jpg", "png");
    $temp = explode(".", $file_name["name"]);
    $extension = end($temp);
    if ((($file_name["type"] == "image/gif")
        || ($file_name["type"] == "image/jpeg")
        || ($file_name["type"] == "image/jpg")
        || ($file_name["type"] == "image/pjpeg" )
        || ($file_name["type"] == "image/x-png" )
        || ($file_name["type"] == "image/png"))
        && ($file_name["size"] < 2000000 ) && in_array($extension, $allowedExts))
    {
        if ($file_name["error"] > 0)
        {
            return "* File Erorr! Return Code: " . $file_name["error"] . "<br>";
        }
        else
        {
            $file_name["name"] = generateRandomString() . "." . $extension;
            while (!file_exists("upload/" . $file_name["name"]))
            {
                $file_name["name"] = generateRandomString() . "." . $extension;
                move_uploaded_file($file_name["tmp_name"], "upload/" . $file_name["name"]);
            }
        }
        return "";
    }
    else
    {
        return "* Invalid or empty picture file.<br>";
    }
}

// Let's get all the user's info
$sql = "SELECT * FROM `gp_users` WHERE `UserName_c` = '" . $login_session . "';";
$result = mysqli_query($db, $sql);
$user_data = mysqli_fetch_array($result, MYSQLI_ASSOC);

if ($_SERVER["REQUEST_METHOD"] == "POST")
{
    // Variables
    $profile_pic = '';
    $pics = '';

    // Validation
    // Profile pic
    $cam_pic_data = $_POST['capture_profile_pic'];
    if ($cam_pic_data != null)
    {
        $cam_pic_data = substr($cam_pic_data, 22); // strip header
        $cam_pic_data = base64_decode($cam_pic_data);
        $profile_pic = generateRandomString() . '.png';
        $im = imagecreatefromstring($cam_pic_data);
        if ($im !== false)
        {
            //header('Content-Type: image/png');
            imagepng($im, 'upload/' . $profile_pic);
            imagedestroy($im);
        }
        else
        {
            echo 'An error occurred.';
        }
    }
    else
    {
        if (isset($_FILES['profile_pic']))
        {
            $profile_pic = $_FILES['profile_pic'];
            $image_error = verify_save_image($profile_pic);
            if ($image_error != "")
            {
                $error.= $image_error;
                $profile_pic = '';
                $is_error = true;
            }
            else
            {
                $profile_pic = $profile_pic['name'];
            }
        }
    }

    // Place pics
    if (isset($_FILES['place_pics']))
    {
        $file_array = reArrayFiles($_FILES['place_pics']);
        foreach ($file_array as $pic)
        {
            $image_error = verify_save_image($pic);
            if ($image_error != "")
            {
                if ($image_error != '* Invalid or empty picture file.<br>') // ignore emtpy picture
                {
                    $error.= $image_error;
                    $profile_pic = '';
                    $is_error = true;
                }
            }
            else
            {
                if ($image_error != '* Invalid or empty picture file.<br>') // ignore empty picture
                {
                    $pics.= $pic['name'] . ',';
                }
            }
        }
        $pics = rtrim($pics, ","); // remove trailing comma
    }

    if ($is_error == false)
    {
        $old_pictures = explode(',', $user_data['Pics_c']);
        $old_profile_pic = $user_data['ProfilePicFile_c'];
        // delete profile pic
        unlink('upload/' . $old_profile_pic);
        // delete all pics
        if (isset($old_pictures) && $old_pictures != null && $old_pictures != '')
        {
            foreach ($old_pictures as &$pic)
            {
                unlink('upload/' . $pic);
            }
        }
        // Update the DB
        $id = $user_data['ID_n'];
        $sql = "
                UPDATE `gp_users` SET
                ProfilePicFile_c = '" . addslashes($profile_pic) . "', 
                Pics_c = '" . addslashes($pics) . "'
                WHERE ID_n='" . addslashes($id) . "'
            ";
        if (mysqli_query($db, $sql))
        {
            header("Location: welcome.php");
        }
        else
        {
            javaAlert("Database Error!");
        }
    }
    else
    {
        // clean up
        // delete profile pic
        if (isset($profile_pic) && $profile_pic != null && $profile_pic != '')
        {
            unlink('upload/' . $profile_pic);
        }
        // delete all pics
        if (isset($pics) && $pics != null && $pics != '')
        {
            $pic_files = explode(',', $pics);
            foreach ($pic_files as &$pic)
            {
                unlink('upload/' . $pic);
            }
        }
    }
}
?>
<!DOCTYPE html>
<html>
    <head>
        <?php include '../head.php'; ?>
        <script>
            /*
             * Function to capture web cam
             */
            function getCamCapture()
            {
                jq.fancybox({
                    type: 'ajax',
                    href: 'camera_interface.php',
                    afterShow: function() {
                        captureVideo();
                    },
                    afterClose: function() {
                        // Stop webcame
                        window.stream.stop();
                    }
                });
            }
            /*
             * Get image from web camera
             */
            function getImage()
            {
                var imageData = jq("#webcam")[0].toDataURL();
                jq('#camContainer').hide();
                jq('#camPreview').show().find('img').attr('src', imageData).width(300);
                var imageData = jq("#webcam")[0].toDataURL();
                jq('#capture_profile_pic').val(imageData);
            }

            function saveCamCapture() {
                jq('#capture_msg').text("(Image captured from web-cam!)");
                jq.fancybox.close();
            }
            function retakeCamCapture()
            {
                jq('#camContainer').show();
                jq('#camPreview').hide();
            }
        </script>
    </head>
    <body>
        <div id="perspective" class="perspective effect-airbnb">
            <?php include '../menu.php'; ?>
            <div class="container">
                <div class="wrapper"><!-- wrapper needed for scroll -->
                    <?php
                    $header_title = 'Group Project - Roomie Track™: Update Images';
                    include '../header.php';
                    ?>
                    <div id="maintable">
                        <table style="width: 100%;">
                            <td class="maintable_info">
                                <!-- sub menu -->
                                <?php include('get_sub_menu.php'); ?>
                                <!-- delete profile -->
                                <h3 class="maintable_info_no_hover blog_box_border blog_box_glass">Update images</h3>
                                <div class="blog_box_with_border">
                                    <br>
                                    <form action="" method="post" name="sign_up_form" enctype="multipart/form-data">
                                        <div style="padding-left: 20px; padding-top: 10px; padding-right: 20px;">
                                            <!-- profile pic -->
                                            <h4 class="in_form">Profile Picture *</h4>
                                            <input id="capture_profile_pic" type="hidden" name="capture_profile_pic"></input>
                                            <input type="button" value="Capture from camera" onclick="getCamCapture();"></input>
                                            <input type="file" name="profile_pic" accept="image/*">
                                            <span id="capture_msg"></span>
                                            <!-- place pictures -->
                                            <h4 class="in_form">Place Pictures (existing images will be replaced, deleted if blank)</h4>
                                            <input type="file" multiple="multiple" name="place_pics[]" accept="image/*">
                                            <input type="file" multiple="multiple" name="place_pics[]" accept="image/*">
                                            <input type="file" multiple="multiple" name="place_pics[]" accept="image/*">
                                            <input type="file" multiple="multiple" name="place_pics[]" accept="image/*">
                                            <br>
                                        </div>
                                        <h4 class="in_form_req_text">(*) Denotes Required Field</h4>
                                        <hr>
                                        <div style="text-align: center;">
                                            <input type="submit" value="Update">
                                        </div>
                                        <br>
                                    </form>
                                    <br>
                                </div>
                                <hr>
                                <p class="maintable_info" style="color: red;" id="validation_area">
                                    <?php echo $error; ?>                                    
                                </p>
                                <p id="display_area">
                                </p>
                            </td>
                        </table>
                    </div>
                    <?php include '../footer.php'; ?>
                </div><!-- wrapper -->
            </div><!-- /container -->
            <?php include '../nav.php'; ?>
        </div><!-- /perspective -->
        <script src="../perspective/js/classie.js"></script>
        <script src="../perspective/js/menu.js"></script>
    </body>
</html>