<?php include('lock.php'); ?>
<!DOCTYPE html>
<html>
    <head>
        <?php
        include '../head.php';

        // Functions
        function echoYesIfTrue($value)
        {
            if ((bool) $value == true)
            {
                echo "Yes";
            }
            else
            {
                echo "No";
            }
        }

        function asDollars($value)
        {
            return '$' . number_format($value, 2);
        }

        // Let's get all the user's info
        $sql = "SELECT * FROM `gp_users` WHERE `UserName_c` = '" . $login_session . "';";
        $result = mysqli_query($db, $sql);
        $user_data = mysqli_fetch_array($result, MYSQLI_ASSOC);
        ?>
    </head>
    <body>
        <div id="perspective" class="perspective effect-airbnb">
            <?php include '../menu.php'; ?>
            <div class="container">
                <div class="wrapper"><!-- wrapper needed for scroll -->
                    <?php
                    $header_title = 'Group Project - Roomie Track™: Home';
                    include '../header.php';
                    ?>
                    <div id="maintable">
                        <table style="width: 100%;">
                            <td class="maintable_info">
                                <!-- sub menu -->
                                <?php include('get_sub_menu.php'); ?>
                                <!-- basic information -->
                                <h3 class="maintable_info_no_hover blog_box_border blog_box_glass">Basic information</h3>
                                <div class="blog_box_with_border">
                                    <br>
                                    <table style='width: 98%; margin-left: auto; margin-right: auto;' cellspacing='0'>
                                        <tr>
                                            <td class='as3' style="width: 138px;">
                                                <a class='fancybox fancybox.image' rel='profile_pics' href='upload/<?php echo($user_data["ProfilePicFile_c"]); ?>'>
                                                    <img class="profile_image" width="128" align="left"
                                                         src="upload/<?php echo($user_data["ProfilePicFile_c"]); ?>" alt="Profile Picture">
                                                </a>
                                            </td>
                                            <td class='as3'>
                                                <table class='shaded_table' style='width: 100%;' cellspacing='0'>
                                                    <tr>
                                                        <td class='as1'>First Name</td>
                                                        <td class='as1'>Last Name</td>
                                                        <td class='as1'>Gender</td>
                                                        <td class='as1'>Email</td>
                                                        <td class='as1'>Job Title</td>
                                                        <td class='as1'>City</td>
                                                        <td class='as1'>State</td>
                                                    </tr>
                                                    <tr>
                                                        <td class='as2'> <?php echo ($user_data["FirstName_c"]); ?> </td>
                                                        <td class='as2'> <?php echo ($user_data["LastName_c"]); ?> </td>
                                                        <td class='as2'> <?php echo ($user_data["Gender_c"]); ?> </td>
                                                        <td class='as2'> <?php echo ($user_data["Email_c"]); ?> </td>
                                                        <td class='as2'> <?php echo ($user_data["JobTitle_c"]); ?> </td>
                                                        <td class='as2'> <?php echo ($user_data["City_c"]); ?> </td>
                                                        <td class='as2'> <?php echo ($user_data["State_c"]); ?> </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                    <br>
                                </div>
                                <hr>
                                <!-- roommate preferences -->
                                <h3 class="maintable_info_no_hover blog_box_border blog_box_glass">Personal habits</h3>
                                <div class="blog_box_with_border">
                                    <br>
                                    <table class='shaded_table' style='width: 98%;' cellspacing='0'>
                                        <tr>
                                            <td class='as1'>Smoking</td>
                                            <td class='as1'>Alcohol</td>
                                            <td class='as1'>Many Visitors</td>
                                            <td class='as1'>Pets Allowed</td>
                                            <td class='as1'>Sleeping Early</td>
                                        </tr>
                                        <tr>
                                            <td class='as2'> <?php echoYesIfTrue($user_data['Smoking_b']); ?> </td>
                                            <td class='as2'> <?php echoYesIfTrue($user_data['Alcohol_b']); ?> </td>
                                            <td class='as2'> <?php echoYesIfTrue($user_data['ManyVisitors_b']); ?> </td>
                                            <td class='as2'> <?php echoYesIfTrue($user_data['PetsAllowed_b']); ?> </td>
                                            <td class='as2'> <?php echoYesIfTrue($user_data['SleepEarly_b']); ?> </td>
                                        </tr>
                                    </table>
                                    <br>
                                </div>
                                <hr>
                                <!-- Property pricing -->
                                <h3 class="maintable_info_no_hover blog_box_border blog_box_glass">Property pricing</h3>
                                <div class="blog_box_with_border">
                                    <br>
                                    <table class='shaded_table' style='width: 98%;' cellspacing='0'>
                                        <tr>
                                            <td class='as1'>Rent</td>
                                            <td class='as1'>Bills</td>
                                        </tr>
                                        <tr>
                                            <td class='as2'> <?php echo(asDollars($user_data['RentPrice_n'])); ?> </td>
                                            <td class='as2'> <?php echo(asDollars($user_data['BillsAmount_n'])); ?> </td>
                                        </tr>
                                    </table>
                                    <br>
                                </div>
                                <hr>
                                <!-- Property attributes -->
                                <h3 class="maintable_info_no_hover blog_box_border blog_box_glass">Property attributes</h3>
                                <div class="blog_box_with_border">
                                    <br>
                                    <table class='shaded_table' style='width: 98%;' cellspacing='0'>
                                        <tr>
                                            <td class='as1'>Washer</td>
                                            <td class='as1'>Dryer</td>
                                            <td class='as1'>Swimming Pool</td>
                                            <td class='as1'>Hardwood Floors</td>
                                            <td class='as1'>Central AC</td>
                                            <td class='as1'>Central Heating</td>
                                            <td class='as1'>Free Parking</td>
                                            <td class='as1'>Game Room</td>
                                            <td class='as1'>Trash Service</td>
                                        </tr>
                                        <tr>
                                            <td class='as2'> <?php echoYesIfTrue($user_data['Washer_b']); ?> </td>
                                            <td class='as2'> <?php echoYesIfTrue($user_data['Dryer_b']); ?> </td>
                                            <td class='as2'> <?php echoYesIfTrue($user_data['SwimmingPool_b']); ?> </td>
                                            <td class='as2'> <?php echoYesIfTrue($user_data['HWFloors_b']); ?> </td>
                                            <td class='as2'> <?php echoYesIfTrue($user_data['CentralAC_b']); ?> </td>
                                            <td class='as2'> <?php echoYesIfTrue($user_data['CentralHeat_b']); ?> </td>
                                            <td class='as2'> <?php echoYesIfTrue($user_data['FreeParking_b']); ?> </td>
                                            <td class='as2'> <?php echoYesIfTrue($user_data['GameRoom_b']); ?> </td>
                                            <td class='as2'> <?php echoYesIfTrue($user_data['TrashService_b']); ?> </td>
                                        </tr>
                                    </table>
                                    <br>
                                </div>
                                <hr>
                                <!-- Property pictures -->
                                <h3 class="maintable_info_no_hover blog_box_border blog_box_glass">Property pictures</h3>
                                <div class="blog_box_with_border">
                                    <br>
                                    <div style="text-align: center;">
                                        <?php
                                        $pictures = explode(',', $user_data['Pics_c']);
                                        if (count($pictures) > 0 && $pictures[0] != '')
                                        {
                                            foreach ($pictures as &$picture)
                                            {
                                                echo "<a class='fancybox fancybox.image' rel='place_pics' href='upload/$picture'>";
                                                echo "<img class='profile_image' style='margin-left: 10px; margin-right: 10px;' width='196' align='center' src='upload/$picture' alt='$picture'>";
                                                echo "</a>";
                                            }
                                        }
                                        else
                                        {
                                            echo "<br><div style='text-align: center;'>No Pictures Uploaded!</div><br>";
                                        }
                                        ?>
                                    </div>
                                    <br>
                                </div>
                                <hr>
                            </td>
                        </table>
                    </div>
                    <?php include '../footer.php'; ?>
                </div><!-- wrapper -->
            </div><!-- /container -->
            <?php include '../nav.php'; ?>
        </div><!-- /perspective -->
        <script src="../perspective/js/classie.js"></script>
        <script src="../perspective/js/menu.js"></script>
    </body>
</html>