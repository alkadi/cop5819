<?php
include('../assign4/config.php');
session_start();

$error = "";
$is_error = false;

// If someone is logged in and is searching, change the menu
$is_logged_in = false;
if (isset($_SESSION['gp_login_user']))
{
    $is_logged_in = true;
}

if ($_SERVER["REQUEST_METHOD"] == "POST")
{
    // Master SQL Query
    $search_query = 'SELECT *, (`RentPrice_n` + `BillsAmount_n`) AS total FROM `gp_users` WHERE ';

    // Expense
    $expense = trim($_POST['expense']);
    if ($expense < 1 || $expense == '' || !ctype_digit($expense) || $expense == null)
    {
        $error.= "* Expense Error, must be provided as a number greater than 0.<br>";
        $is_error = true;
    }
    else
    {
        // Valud add to search
        $search_query.= '(`RentPrice_n` + `BillsAmount_n` <= "' . addslashes($expense) . '") ';
    }

    // City
    $city = '';
    if ($_POST['city'] != '')
    {
        $city = trim($_POST['city']);
        $search_query.= 'AND `City_c` = "' . addslashes($city) . '" ';
    }

    // State
    $state = $_POST['DD_states'];
    if ($state == '' || ctype_digit($state) || $state == null)
    {
        $error.= "* State Error, must be provided with no numbers.<br>";
        $is_error = true;
    }
    else
    {
        $search_query.= 'AND `State_c` = "' . addslashes($state) . '" ';
    }

    // Sleeping habits, sleep_early sleep_late
    if ($_POST['sleep_habit'] != "any")
    {
        if ($_POST['sleep_habit'] == "early")
        {
            $search_query.= 'AND `SleepEarly_b` = 1 ';
        }
        else
        {
            $search_query.= 'AND `SleepLate_b` = 1 ';
        }
    }

    // Visitors habits, manu, not_many
    if ($_POST['visitors'] != "any")
    {
        if ($_POST['visitors'] == "many")
        {
            $search_query.= 'AND `ManyVisitors_b` = 1 ';
        }
        else
        {
            $search_query.= 'AND `NotManyVisitors_b` = 1 ';
        }
    }

    // Smoking habits, yes, no
    if ($_POST['smoking'] != "any")
    {
        if ($_POST['smoking'] == "yes")
        {
            $search_query.= 'AND `Smoking_b` = 1 ';
        }
        else
        {
            $search_query.= 'AND `NonSmoking_b` = 1 ';
        }
    }

    // Alcohol habits, yes, no
    if ($_POST['alcohol'] != "any")
    {
        if ($_POST['alcohol'] == "yes")
        {
            $search_query.= 'AND `Alcohol_b` = 1 ';
        }
        else
        {
            $search_query.= 'AND `NoAlcohol_b` = 1 ';
        }
    }

    // Check boxes
    if (isset($_POST['checkboxes']))
    {
        // Loop and update
        foreach ($_POST['checkboxes'] as $box)
        {
            switch ($box)
            {
                case 'pets_allowed': $search_query.= 'AND `PetsAllowed_b` = 1 ';
                    break;
                case 'washer': $search_query.= 'AND `Washer_b` = 1 ';
                    break;
                case 'dryer': $search_query.= 'AND `Dryer_b` = 1 ';
                    break;
                case 'swimming_pool': $search_query.= 'AND `SwimmingPool_b` = 1 ';
                    break;
                case 'hw_floors': $search_query.= 'AND `HWFloors_b` = 1 ';
                    break;
                case 'central_ac': $search_query.= 'AND `CentraAC_b` = 1 ';
                    break;
                case 'central_heat': $search_query.= 'AND `CentralHeat_b` = 1 ';
                    break;
                case 'free_parking': $search_query.= 'AND `FreeParking_b` = 1 ';
                    break;
                case 'game_room': $search_query.= 'AND `GameRoom_b` = 1 ';
                    break;
                case 'trash_service': $search_query.= 'AND `TrashService_b` = 1 ';
                    break;
            }
        }
    }

    // only if validation passes
    if ($is_error == false)
    {
        $search_query.=" ORDER BY total";
        $result = mysqli_query($db, $search_query);
        $results_array = array(); // make a new array to hold data
        $index = 0;
        while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) // loop to give the data in an associative array
        {
            $results_array[$index] = $row;
            $index++;
        }
        $_SESSION['search_results'] = $results_array;
        header("Location: search_results.php");
    }
}
?>
<!DOCTYPE html>
<html>
    <head>
        <?php include '../head.php'; ?>
        <script>
            /*
             * Function to clear form
             */
            function clearTextArea()
            {
                document.getElementById('validation_area').innerHTML = "";
            } // End of function
        </script>
    </head>
    <body>
        <div id="perspective" class="perspective effect-airbnb">
            <?php include '../menu.php'; ?>
            <div class="container">
                <div class="wrapper"><!-- wrapper needed for scroll -->
                    <?php
                    $header_title = 'Group Project - Roomie Track™: Search';
                    include '../header.php';
                    ?>
                    <div id="maintable">
                        <table style="width: 100%;">
                            <td class="maintable_info">
                                <?php
                                if ($is_logged_in == true)
                                {
                                    include('get_sub_menu.php');
                                }
                                else
                                {
                                    include('get_main_app_menu.php');
                                }
                                ?>
                                <h3 class="maintable_info_no_hover blog_box_border blog_box_glass">What would you like to search for?</h3>
                                <div class="blog_box_with_border">
                                    <br>
                                    <form action="" method="post" name="login_form">
                                        <div style="padding-left: 20px; padding-top: 10px; padding-right: 20px;">
                                            <!-- total expense -->
                                            <h4 class="in_form">Maximum Shared Expense *</h4>
                                            <input type="text" name="expense">
                                            <!-- city -->
                                            <h4 class="in_form">City </h4>
                                            <input type="text" name="city" value="">
                                            <!-- state -->
                                            <h4 class="in_form">State *</h4>
                                            <?php include('get_states_list.php'); ?>
                                            <!-- speel habit -->
                                            <h4 class="in_form">Sleeping Habits *</h4> 
                                            <input type="radio" name="sleep_habit" value="any" checked>Any
                                            <input type="radio" name="sleep_habit" value="early">Sleep early
                                            <input type="radio" name="sleep_habit" value="late">Sleep late
                                            <!-- visitors habits -->
                                            <h4 class="in_form">Visitors Habits *</h4> 
                                            <input type="radio" name="visitors" value="any" checked>Any
                                            <input type="radio" name="visitors" value="many">Many visitors
                                            <input type="radio" name="visitors" value="not_many">Not many visitors
                                            <!-- smoking habits -->
                                            <h4 class="in_form">Smoking Habits *</h4> 
                                            <input type="radio" name="smoking" value="any" checked>Any
                                            <input type="radio" name="smoking" value="yes">Yes
                                            <input type="radio" name="smoking" value="no">No
                                            <!-- alcohol habits -->
                                            <h4 class="in_form">Alcohol Habits *</h4> 
                                            <input type="radio" name="alcohol" value="any" checked>Any
                                            <input type="radio" name="alcohol" value="yes">Alcohol allowed
                                            <input type="radio" name="alcohol" value="no">Alcohol not allowed
                                            <!-- check boxes -->
                                            <h4 class="in_form">Select all that applies</h4>
                                            <table style="margin-top: 0; margin-bottom: 0;">
                                                <tr>
                                                    <td><input type="checkbox" name="checkboxes[]" value="pets_allowed">Pets allowed</td>
                                                    <td><input type="checkbox" name="checkboxes[]" value="washer">Washer</td>
                                                    <td><input type="checkbox" name="checkboxes[]" value="dryer">Dryer</td>
                                                    <td><input type="checkbox" name="checkboxes[]" value="swimming_pool">Swimming pool</td>
                                                    <td><input type="checkbox" name="checkboxes[]" value="hw_floors">Hard wood floors</td>
                                                </tr>
                                                <tr>
                                                    <td><input type="checkbox" name="checkboxes[]" value="central_ac">Central AC</td>
                                                    <td><input type="checkbox" name="checkboxes[]" value="central_heat">Central heating</td>
                                                    <td><input type="checkbox" name="checkboxes[]" value="free_parking">Free parking</td>
                                                    <td><input type="checkbox" name="checkboxes[]" value="game_room">Game room</td>
                                                    <td><input type="checkbox" name="checkboxes[]" value="trash_service">Trash service</td>
                                                </tr>
                                            </table>
                                        </div>
                                        <h4 class = "in_form_req_text">(*) Denotes Required Field</h4>
                                        <hr>
                                        <div style = "text-align: center;">
                                            <input type = "submit" value = "Search">
                                            <input type = "reset" value = "Reset" onclick = "clearTextArea();">
                                        </div>
                                        <br>
                                    </form>
                                    <br>
                                </div>
                                <hr>
                                <p class = "maintable_info" style = "color: red;" id = "validation_area">
                                    <?php echo $error;
                                    ?>                                    
                                </p>
                                <p id="display_area">
                                </p>
                            </td>
                        </table>
                    </div>
                    <?php include '../footer.php'; ?>
                </div><!-- wrapper -->
            </div><!-- /container -->
            <?php include '../nav.php'; ?>
        </div><!-- /perspective -->
        <script src="../perspective/js/classie.js"></script>
        <script src="../perspective/js/menu.js"></script>
    </body>
</html>