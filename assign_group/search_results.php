<?php
session_start();

// If someone is logged in and is searching, change the menu
$is_logged_in = false;
if (isset($_SESSION['gp_login_user']))
{
    $is_logged_in = true;
}

// Get search results
$search_results = '';
if (isset($_SESSION['search_results']))
{
    $search_results = $_SESSION['search_results'];
}
else
{
    header("Location: search.php");
}
?>

<!DOCTYPE html>
<html>
    <head>
        <?php
        include '../head.php';

        // Functions
        function tickIfTrue($value)
        {
            if ((bool) $value == true)
            {
                return "✓";
            }
            else
            {
                return "&nbsp;"; // non-breaking white space
            }
        }

        function asDollars($value)
        {
            return '$' . number_format($value, 2);
        }
        ?>
    </head>
    <body>
        <div id="perspective" class="perspective effect-airbnb">
            <?php include '../menu.php'; ?>
            <div class="container">
                <div class="wrapper"><!-- wrapper needed for scroll -->
                    <?php
                    $header_title = 'Group Project - Roomie Track™: Search Results';
                    include '../header.php';
                    ?>
                    <div id="maintable">
                        <table style="width: 100%;">
                            <td class="maintable_info">
                                <!-- sub menu -->
                                <?php
                                if ($is_logged_in == true)
                                {
                                    include('get_sub_menu.php');
                                }
                                else
                                {
                                    include('get_main_app_menu.php');
                                }
                                ?>
                                <!-- results -->
                                <?php
                                if (count($search_results) == 0)
                                {
                                    echo "<h3 class='maintable_info_no_hover blog_box_border blog_box_glass'>";
                                    echo "No results";
                                    echo "</h3>";
                                    echo "<div class='blog_box_with_border'>";
                                    echo "  <br>";
                                    echo "  <div style='text-align: center;'>";
                                    echo "      Nothing found! <a href='search.php' title='Search'>click here</a> to search for something else!'";
                                    echo "  </div>";
                                    echo "  <br>";
                                    echo "</div>";
                                    echo "<hr>";
                                }
                                for ($i = 0; $i < count($search_results); ++$i)
                                {
                                    // All the info
                                    $ar = $search_results[$i];
                                    $profile_pic = $ar['ProfilePicFile_c'];
                                    $first_name = $ar['FirstName_c'];
                                    $last_name = $ar['LastName_c'];
                                    $gender = $ar['Gender_c'];
                                    $job_title = $ar['JobTitle_c'];
                                    $email = $ar['Email_c'];
                                    $pictures = explode(',', $ar['Pics_c']);
                                    $city = $ar['City_c'];
                                    $state = $ar['State_c'];
                                    $j = $i + 1; // Human readable index
                                    // Habits and money
                                    $rent_price = $ar['RentPrice_n'];
                                    $bills_amount = $ar['BillsAmount_n'];
                                    $sleep = (bool) $ar['SleepLate_b'];
                                    $smoking = (bool) $ar['Smoking_b'];
                                    $many_visitors = (bool) $ar['ManyVisitors_b'];
                                    $alcohol = (bool) $ar['Alcohol_b'];
                                    $pet_ok = (bool) $ar['PetsAllowed_b'];
                                    // Features
                                    $washer = (bool) $ar['Washer_b'];
                                    $dryer = (bool) $ar['Dryer_b'];
                                    $swimming_pool = (bool) $ar['SwimmingPool_b'];
                                    $hw_floors = (bool) $ar['HWFloors_b'];
                                    $central_ac = (bool) $ar['CentralAC_b'];
                                    $central_heat = (bool) $ar['CentralHeat_b'];
                                    $free_parking = (bool) $ar['FreeParking_b'];
                                    $game_room = (bool) $ar['GameRoom_b'];
                                    $trash_service = (bool) $ar['TrashService_b'];
                                    $email_icon = "<img style='margin: 0;' height='36' align='center' src='email-icon.png' alt='$email'>";
                                    $email_string_icon = "<a href='mailto:$email?Subject=Interest from Roomie Track™' target='_blank' title='$email'>$email_icon</a>";

                                    echo "<h3 class='maintable_info_no_hover blog_box_border blog_box_glass'>";
                                    echo "$j - $first_name $last_name [$gender] $job_title - $city $state";
                                    echo "</h3>";
                                    echo "<div class='blog_box_with_border'>";
                                    echo "<br>";
                                    echo "<table style='width: 98%; margin-left: 0; margin-right: auto;' cellspacing='0'>";
                                    echo "  <tr>";
                                    echo "      <td class='as3' style='width: 138px;'>";
                                    echo "          <a class='fancybox fancybox.image' rel='result$i' href='upload/$profile_pic'>";
                                    echo "          <img class='profile_image' style='margin-left: 10px; margin-right: 10px;' height='96' align='center' src='upload/$profile_pic' alt='$profile_pic'>";
                                    echo "          </a>";
                                    echo "      </td>";
                                    echo "      <td class='as3'>";
                                    echo "          <table class='shaded_table' style='width: 100%; margin-bottom: 5px;' cellspacing='0'>";
                                    echo "              <tr>";
                                    echo "                  <td rowspan = '2' class='as1'>$email_string_icon</td>";
                                    echo "                  <td class='as1'>Rent</td>";
                                    echo "                  <td class='as1'>Bills</td>";
                                    echo "                  <td class='as1'>Total</td>";
                                    echo "                  <td class='as1'>Late Sleeper</td>";
                                    echo "                  <td class='as1'>Smoker</td>";
                                    echo "                  <td class='as1'>Alcohol</td>";
                                    echo "                  <td class='as1'>Many Visitors</td>";
                                    echo "                  <td class='as1'>Pets OK</td>";
                                    echo "              </tr>";
                                    echo "              <tr>";
                                    echo "                  <td class='as2'>" . asDollars($rent_price) . "</td>";
                                    echo "                  <td class='as2'>" . asDollars($bills_amount) . "</td>";
                                    echo "                  <td class='as2'>" . asDollars($rent_price + $bills_amount) . "</td>";
                                    echo "                  <td class='as2'>" . tickIfTrue($sleep) . "</td>";
                                    echo "                  <td class='as2'>" . tickIfTrue($smoking) . "</td>";
                                    echo "                  <td class='as2'>" . tickIfTrue($alcohol) . "</td>";
                                    echo "                  <td class='as2'>" . tickIfTrue($many_visitors) . "</td>";
                                    echo "                  <td class='as2'>" . tickIfTrue($pet_ok) . "</td>";
                                    echo "              </tr>";
                                    echo "          </table>";
                                    echo "          <table class='shaded_table' style='width: 100%;' cellspacing='0'>";
                                    echo "              <tr>";
                                    echo "                  <td class='as1'>Washer</td>";
                                    echo "                  <td class='as1'>Dryer</td>";
                                    echo "                  <td class='as1'>Pool</td>";
                                    echo "                  <td class='as1'>Wood Floors</td>";
                                    echo "                  <td class='as1'>AC</td>";
                                    echo "                  <td class='as1'>Heat</td>";
                                    echo "                  <td class='as1'>Free Parking</td>";
                                    echo "                  <td class='as1'>Game Room</td>";
                                    echo "                  <td class='as1'>Trash Service</td>";
                                    echo "              </tr>";
                                    echo "              <tr>";
                                    echo "                  <td class='as2'>" . tickIfTrue($washer) . "</td>";
                                    echo "                  <td class='as2'>" . tickIfTrue($dryer) . "</td>";
                                    echo "                  <td class='as2'>" . tickIfTrue($swimming_pool) . "</td>";
                                    echo "                  <td class='as2'>" . tickIfTrue($hw_floors) . "</td>";
                                    echo "                  <td class='as2'>" . tickIfTrue($central_ac) . "</td>";
                                    echo "                  <td class='as2'>" . tickIfTrue($central_heat) . "</td>";
                                    echo "                  <td class='as2'>" . tickIfTrue($free_parking) . "</td>";
                                    echo "                  <td class='as2'>" . tickIfTrue($game_room) . "</td>";
                                    echo "                  <td class='as2'>" . tickIfTrue($trash_service) . "</td>";
                                    echo "              </tr>";
                                    echo "          </table>";
                                    echo "      </td>";
                                    echo "  </tr>";
                                    if (count($pictures) > 0 && $pictures[0] != '')
                                    {
                                        echo "  <tr>";
                                        echo "      <td colspan='2'>";
                                        echo "          <div style='text-align: left;'>";
                                        echo "          <h5 style='text-align: left; margin: 10px;'> Property Photos</h5>";
                                        foreach ($pictures as &$pic)
                                        {
                                            echo "<a class='fancybox fancybox.image' rel='place_pics$i' href='upload/$pic'>";
                                            echo "<img class='profile_image' style='margin-left: 10px; margin-right: 10px;' height='96' align='center' src='upload/$pic' alt='$pic'>";
                                            echo "</a>";
                                        }
                                        echo "          </div>";
                                        echo "      </td>";
                                        echo "  </tr>";
                                    }
                                    echo "</table>";
                                    echo "<br>";
                                    echo "</div>";
                                    echo "<hr>";
                                }
                                ?>
                            </td>
                        </table>
                    </div>
                    <?php include '../footer.php'; ?>
                </div><!-- wrapper -->
            </div><!-- /container -->
            <?php include '../nav.php'; ?>
        </div><!-- /perspective -->
        <script src="../perspective/js/classie.js"></script>
        <script src="../perspective/js/menu.js"></script>
    </body>
</html>