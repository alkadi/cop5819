<title>Al Alkad - COP 5819</title>
<meta charset="UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1"> 
<link rel="stylesheet" type="text/css" href="/~n00644504/cop4813/mystyle.css">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="shortcut icon" href="/~n00644504/cop4813/favicon.ico" type="image/x-icon">
<link rel="icon" href="/~n00644504/cop4813/favicon.ico" type="image/x-icon">
<link rel="stylesheet" type="text/css" href="/~n00644504/cop4813/perspective/css/component.css" />
<script src="/~n00644504/cop4813/perspective/js/modernizr.custom.25376.js"></script>
<script type="text/javascript" src="http://code.jquery.com/jquery-latest.min.js"></script>
<link rel="stylesheet" href="/~n00644504/cop4813/fancybox/source/jquery.fancybox.css?v=2.1.5" type="text/css" media="screen" />
<script type="text/javascript" src="/~n00644504/cop4813/fancybox/source/jquery.fancybox.pack.js?v=2.1.5"></script>
<link rel="stylesheet" href="/~n00644504/fancybox/source/helpers/jquery.fancybox-buttons.css?v=1.0.5" type="text/css" media="screen" />
<script type="text/javascript" src="/~n00644504/fancybox/source/helpers/jquery.fancybox-buttons.js?v=1.0.5"></script>
<script type="text/javascript" src="/~n00644504/fancybox/source/helpers/jquery.fancybox-media.js?v=1.0.6"></script>
<link rel="stylesheet" href="/~n00644504/fancybox/source/helpers/jquery.fancybox-thumbs.css?v=1.0.7" type="text/css" media="screen" />
<script type="text/javascript" src="/~n00644504/fancybox/source/helpers/jquery.fancybox-thumbs.js?v=1.0.7"></script>
<script type="text/javascript">
    var jq = jQuery.noConflict();
    jq("document").ready(function()
    {
        jq(window).scroll(function()
        {
            if (jq(this).scrollTop() > -1)
            {
                jq('.nav-container').addClass("f-nav");
            }
            else
            {
                jq('.nav-container').removeClass("f-nav");
            }
            jq(".fancybox").fancybox({
                preLoad: true
            }).showLoading();
        });
    });
</script>