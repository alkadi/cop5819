<?php include('../assign4/lock.php'); ?>
<!DOCTYPE html>
<html>
    <head>
        <?php include '../head.php'; ?>
        <script>
            function showData(str)
            {
                if (str == "")
                {
                    document.getElementById("text_area").innerHTML = "";
                    return;
                }
                if (window.XMLHttpRequest)
                {
                    // code for IE7+, Firefox, Chrome, Opera, Safari
                    xmlhttp = new XMLHttpRequest();
                }
                else
                { // code for IE6, IE5
                    xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
                }
                xmlhttp.onreadystatechange = function()
                {
                    if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
                    {
                        document.getElementById("text_area").innerHTML = xmlhttp.responseText;
                    }
                }
                xmlhttp.open("GET", "getdata.php?q=" + str, true);
                xmlhttp.send();
            }
        </script>        
    </head>
    <body>
        <div id="perspective" class="perspective effect-airbnb">
            <?php include '../menu.php'; ?>
            <div class="container">
                <div class="wrapper"><!-- wrapper needed for scroll -->
                    <?php
                    $header_title = 'Assignment 6 - Simple AJAX Demo: Home';
                    include '../header.php';
                    ?>
                    <div id="maintable">
                        <table style="width: 100%;">
                            <td class="maintable_info">
                                <?php include('get_sub_menu.php'); ?>
                                <h3 class="maintable_info_no_hover blog_box_border blog_box_glass">AJAX Demo</h3>
                                <div class="blog_box_with_border">
                                    <br>
                                    <form action="" method="post" name="delete_entry_form">
                                        <div style="padding-left: 20px; padding-top: 10px; padding-right: 20px;">
                                            <h4 class="in_form">Select entry to display</h4>
                                            <select name="current_entry" style="width: 100%;" onchange="showData(this.value)">
                                                <?php
                                                $sql = "SELECT 1 FROM persons;";
                                                $val = mysqli_query($db, $sql);
                                                if ($val !== FALSE)
                                                {
                                                    // Exists
                                                    $query = "SELECT * FROM persons;";
                                                    $result = mysqli_query($db, $query);
                                                    echo "<option selected='selected' value=' '></option>";
                                                    while ($row = mysqli_fetch_array($result, MYSQLI_NUM))
                                                    {
                                                        echo "<option value='" . $row[0] . "'>"
                                                        . $row[0] . " - "
                                                        . $row[1] . " "
                                                        . $row[2] . "</option>";
                                                    }
                                                }
                                                ?>
                                            </select>
                                            <br>
                                            <br>
                                        </div>
                                        <h4 class="in_form_req_text"><!--(*) Denotes Required Field--></h4>
                                    </form>
                                    <br>
                                </div>
                                <hr>
                                <h3 class='maintable_info_no_hover blog_box_border blog_box_glass'>
                                    Selected Entry Data</h3>
                                <div class='blog_box_with_border'>
                                    <br>
                                    <div id='text_area' style='text-align: center;'>
                                        Nothing Selected!
                                    </div>
                                    <br>
                                </div>
                                <hr>
                                <p class="maintable_info" style="color: red;" id="validation_area">
                                </p>
                                <p id="display_area">
                                </p>
                            </td>
                        </table>
                    </div>
                    <?php include '../footer.php'; ?>
                </div><!-- wrapper -->
            </div><!-- /container -->
            <?php include '../nav.php'; ?>
        </div><!-- /perspective -->
        <script src="../perspective/js/classie.js"></script>
        <script src="../perspective/js/menu.js"></script>
    </body>
</html>